//debug
//if (!jQuery) {
//	var jQuery,$,console,window;
//}
//debugEnd
(function($) {
	$.fn.rsLayout = function(options) {
		var opt = $.extend({
			topHeight		:"",
			bottomHeight	:"",
			leftWidth		:"",
			rightWidth		:"",
			topResizable	:false,
			leftResizable	:false,
			rightResizable	:false,
			bottomResizable	:false,
			topMinHeight	:10,
			leftMinWidth	:100,
			rightMinWidth	:100,
			bottomMinHeight	:10,
			topMaxHeight	:100,
			leftMaxWidth	:600,
			rightMaxWidth	:1000,
			bottomMaxHeight	:1000,
			wrapper			:false
		},options);
		var def = {
			wrap   :".rs_layout",
			top    :".rs_layout_top",
			left   :".rs_layout_left",
			right  :".rs_layout_right",
			center :".rs_layout_center",
			right_nr :".designer_k",
			left_nr :".lf_ct",
			bottom :".rs_layout_bottom"
		};

		var _win = $(window);
		_win.off("resize");
		// !!!function 计算窗体高度
		function calcWinHeight(_win) {
			var wh = _win.height();
			return wh;
		}
		function setWrapLayoutHeight(obj,calcHeight) {
			obj.css("height", calcHeight);
		}
		function setCenterHeight(obj,cHeight){
			obj.css("height", cHeight);
		}
		function setCenterPos(obj,_self,cleft,cright){
			var intCleft,intCright,intCtop;
			if (cleft.length > 0) { intCleft = cleft.outerWidth();} else { intCleft = "0";}
			if (cright.length > 0) { intCright = cright.outerWidth();} else {intCright = "0";}
			obj.css({
				"width"	: _self.width() - intCleft - intCright,
				"marginLeft"	: intCleft,
				"marginRight"	: intCright
			});
			//console.log(intCleft,intCright);
		}
		//遍历每个对象
		return this.each(function(index) {
			var _self    = $(this);
			var _top     = _self.children(def.top);
			var _left    = _self.children(def.left);
			var _right   = _self.children(def.right);
			var _center  = _self.children(def.center);
			var _bottom  = _self.children(def.bottom);
			var _megeCen = $.merge($.merge($.merge($(),_left), _center),_right);
			
			var topResize,
				leftResize,
				rightResize,
				bottomResize;
			
			if (_top.attr("resizable")=="true"||opt.topResizable===true) {topResize=true;}
			if (_left.attr("resizable")=="true"||opt.leftResizable===true) {leftResize=true;}
			if (_right.attr("resizable")=="true"||opt.rightResizable===true) {rightResize=true;}
			if (_bottom.attr("resizable")=="true"||opt.bottomResizable===true) {bottomResize=true;}
			
			if (opt.topHeight)		{def.top.css("height", opt.topHeight);}
			if (opt.bottomHeight)	{def.bottom.css("height", opt.bottomHeight);}
			if (opt.leftWidth)		{def.left.css("width", opt.leftWidth);}
			if (opt.rightWidth)		{def.right.css("width", opt.rightWidth);}
			
			if (leftResize===true && !_left.children().first().hasClass("resizeBar")) {
				var sizeBar = $("<div class='resizeBar' />");
				var inner =$("<div class='leftInner'/>");
				_left.css("width", "+=4px").children().wrapAll(inner).end()
					.prepend(sizeBar).end();
				sizeBar.on("mousedown", function(e) {
					e.stopPropagation();
					if (_left.hasClass("leftminsize")) {return;}
					var _this = $(this);
					var _pos  = _this.position();
					var nPageX = e.pageX;
					var _clones =_this.clone().css({
						zIndex:9999,
						borderColor:"#0073c7",
						backgroundColor:"#0073c7",
						position:"absolute",
						left:_pos.left,
						top:_pos.top
					});
					_this.before(_clones);
					function objMove(obj,_pos,oldX,newX) {
						obj.css({
							left: Number(newX - obj.outerWidth()/2 )
						});
					}
					$(document).on("mousemove.objMove", function(e) {
						var npx = e.pageX;
						objMove(_clones,_pos,nPageX,npx);
						e.preventDefault();
						return false;
					});
					_clones.on({
						mouseup:function(e) {
							e.stopPropagation();
							_left.css("width", _clones.css("left") );
							_clones.remove();
							$(document).off("mousemove.objMove");
							if ( opt.leftMinWidth && _left.width() < opt.leftMinWidth ) {
								_left.width(opt.leftMinWidth);
							}
							if ( opt.leftMaxWidth && _left.width() > opt.leftMaxWidth ) {
								_left.width(opt.leftMaxWidth);
							}
							_win.resize();
							if (treeNiceScroll) {
								treeNiceScroll.resize();
							}
						}
					});
					
				});
			}
			
			
			if (rightResize===true && !_right.children().first().hasClass("resizeBar")) {
				var sizeBar = $("<div class='resizeBar' />");
				var inner =$("<div class='rightInner'/>");
				if (parseInt(_right.css("borderLeftWidth")) > 0 ) {
					_right.css("borderLeftWidth","0px")
				}
				
				_right.css("width", "+=4px").children().wrapAll(inner).end()
					.prepend(sizeBar).end();
				
				sizeBar.on("mousedown", function(e) {
					e.stopPropagation();
					if (_right.hasClass("rightminsize")) {return;}
					var _this = $(this);
					var _pos  = _this.position();
					var nPageX = e.pageX;
					var _clones =_this.clone().css({
						zIndex:9999,
						borderColor:"#0073c7",
						backgroundColor:"#0073c7",
						position:"fixed",
						left:_pos.left,
						top:_pos.top
					});
					_this.before(_clones);
					function objMove(obj,_pos,oldX,newX) {
						obj.css({
							left: Number(newX - obj.outerWidth()/2 )
						});
					}
					$(document).on("mousemove.objMove", function(e) {
						var npx = e.pageX;
						objMove(_clones,_pos,nPageX,npx);
						e.preventDefault();
						return false;
					});
					_clones.on({
						mouseup:function(e) {
							e.stopPropagation();
							var getLength = (function() {
								return _right.offset().left - _clones.offset().left + _right.width() ;
							})(_right,_clones);
							
							
							
							_right.css("width",getLength );
							
							
							_clones.remove();
							$(document).off("mousemove.objMove");
							if ( opt.rightMinWidth && _right.width() < opt.rightMinWidth ) {
								_right.width(opt.rightMinWidth);
							}
							if ( opt.rightMaxWidth && _right.width() > opt.rightMaxWidth ) {
								_right.width(opt.rightMaxWidth);
							}
							_win.resize();
							if (treeNiceScroll) {
								treeNiceScroll.resize();
							}
						}
					});
					
				});
			}
			
			
			if (bottomResize===true && !_bottom.children().first().hasClass("resizeBar")) {
				var sizeBar = $("<div class='resizeBar' />");
				var inner =$("<div class='bottomInner'/>");
				_bottom.css({"height": "+=4px"}).children().wrapAll(inner).end()
					.prepend(sizeBar).end();
				var botinner = _bottom.children(".bottomInner");
				botinner.height( _bottom.height() - 4 );
				sizeBar.on("mousedown", function(e) {
					e.stopPropagation();
					if (_bottom.hasClass("bottomminsize")) {return;}
					var _this = $(this);
					var _pos  = _this.position();
					var nPageX = e.pageY;
					var _clones =_this.clone().css({
						zIndex:9999,
						borderColor:"#0073c7",
						backgroundColor:"#0073c7",
						position:"fixed",
						left:_pos.left,
						top:_pos.top
					});
					_this.before(_clones);
					function objMove(obj,_pos,oldX,newX) {
						obj.css({
							top: Number(newX - obj.outerHeight()/2 )
						});
					}
					$(document).on("mousemove.objMoveY", function(e) {
						var npx = e.pageY;
						objMove(_clones,_pos,nPageX,npx);
						e.preventDefault();
						return false;
					});
					_clones.on({
						mouseup:function(e) {
							e.stopPropagation();
							var getLength = (function() {
								return _bottom.offset().top - _clones.offset().top + _bottom.height() ;
							})(_bottom,_clones);
							
							_bottom.css("height", getLength );
							_clones.remove();
							$(document).off("mousemove.objMoveY");
							if ( opt.bottomMinHeight && _bottom.height() < opt.bottomMinHeight ) {
								_bottom.height(opt.bottomMinHeight);
							}
							if ( opt.bottomMaxHeight && _bottom.height() > opt.bottomMaxHeight ) {
								_bottom.height(opt.bottomMaxHeight);
							}
							botinner.height( _bottom.height() - 4 );
							_win.resize();
							if (treeNiceScroll) {
								treeNiceScroll.resize();
							}
						}
					});
					
				});
			}
			var topMinsize,
				leftMinsize,
				rightMinsize,
				bottomMinsize;
			
			if (_top.attr("minsize")=="true"||opt.topMinsize===true) {topMinsize=true;}
			if (_left.attr("minsize")=="true"||opt.leftMinsize===true) {leftMinsize=true;}
			if (_right.attr("minsize")=="true"||opt.rightMinsize===true) {rightMinsize=true;}
			if (_bottom.attr("minsize")=="true"||opt.bottomMinsize===true) {bottomMinsize=true;}
			
			if (leftMinsize === true) {
				var minsizeBar = $("<div>").addClass("minsize");
				var minObj = _left.children(".leftInner");
				var minbar;
				var oldSizeL;
				_left.css("position","relative");
				minObj.css("overflow","hidden");
				_left.append(minsizeBar);
				minbar = _left.children(".minsize");
				minbar.on("click", function() {
					var nowWidth = _left.width();
					if ( nowWidth == "4") {
						if (!oldSizeL) {
							_left.width(opt.leftMinWidth);
						} else {
							_left.width(oldSizeL);
						}
						_left.removeClass("leftminsize");
					} else {
						oldSizeL = _left.width();
						_left.addClass("leftminsize").width(4);
					}
					
					_win.resize();
				});
				
			}
			
			if (rightMinsize === true) {
				var minsizeBar = $("<div>").addClass("minsize");
				var minObj = _right.children(".rightInner");
				var minbar;
				var oldSizeL;
				_right.css("position","relative");
				minObj.css("overflow","hidden");
				_right.append(minsizeBar);
				minbar = _right.children(".minsize");
				minbar.on("click", function() {
					var nowWidth = _right.width();
					if ( nowWidth == "4") {
						if (!oldSizeL) {
							_right.width(opt.leftMinWidth);
						} else {
							_right.width(oldSizeL);
						}
						_right.removeClass("rightminsize");
					} else {
						oldSizeL = _right.width();
						_right.addClass("rightminsize").width(4);
					}
					
					_win.resize();
				});
				
			}
			
			
			if (bottomMinsize === true) {
				var minsizeBar = $("<div>").addClass("minsize");
				var minObj = _bottom.children(".bottomInner");
				var minbar;
				var oldSizeL;
				_bottom.css("position","relative");
				//minObj.css("overflow","hidden");
				_bottom.append(minsizeBar);
				minbar =	_bottom.children(".minsize");
				minbar.on("click", function() {
					var nowWidth =	_bottom.height();
					if ( nowWidth == "4") {
						if (!oldSizeL) {
							_bottom.height(opt.bottomMinHeight);
						} else {
							_bottom.height(oldSizeL);
						}
						_bottom.removeClass("bottomminsize");
						minObj.removeAttr("style");
					} else {
			
						oldSizeL =	_bottom.height();
						_bottom.addClass("bottomminsize")//.height(4);
					}
					
					_win.resize();
				});
				
			}
			
			if (_self.attr("wrapper")=="true"||opt.wrapper===true){
				$("html,body").css("overflow","hidden");
				_win.on("ready resize", function() {
					setWrapLayoutHeight(_self,calcWinHeight(_win));
				});
			} else {
				setWrapLayoutHeight(_self,"100%");  
			}
			
			_win.on("ready resize", function() {
				setCenterPos(_center,_self,_left,_right);
				setCenterHeight(_megeCen, function(){
					return _self.height() - _top.outerHeight() - _bottom.outerHeight();
				});
			});
		});
	};
	//--
	$.fn.rs_simpleTabs = function(options) {
		var opt = $.extend({
			tabElem				:"",		//切换标签的子元素
			tabCurrent			:"cur",		//切换标签的Current状态
			tabCont				:"",		//切换的元素内容父级
			tabContLayer		:"",		//切换的元素内容层
			tabContLayerShow	:"show",	//切换的元素Current状态
			tabCloseBtn			:".tabClosed",
			afclick				:null		//回调函数接口			
		},options);
		return this.each(function() {
			var _this			= $(this);
			var _tabElem		= _this.children(opt.tabElem);
			var _tabCont		= $(opt.tabCont);
			var _tabContLayer	= _tabCont.children(opt.tabContLayer);
			_this.on("click", opt.tabElem, function(e) {
				e.stopPropagation();
				var _this = $(this);
				var _index = _this.index();
				var _thiscur = _tabElem.filter("."+ opt.tabCurrent);
				if ( _thiscur[0] == _this[0] ) { return; } else {
					_thiscur.removeClass(opt.tabCurrent);
					_this.addClass(opt.tabCurrent);
					_tabContLayer
						.filter("."+opt.tabContLayerShow)
						.removeClass(opt.tabContLayerShow)
						.end()
						.eq(_index).addClass(opt.tabContLayerShow);
					if (opt.afclick) { opt.afclick(_index); }
				}
			}).on("click", opt.tabCloseBtn, function(e){
				e.stopPropagation();
				if ( _this.children(opt.tabElem).length == 1) {
					return;
				}
				var _thisx = $(this);
				var _paret = _thisx.parent();
				var _pIndex = _paret.index();
				_paret.remove();
				_tabContLayer.eq(_pIndex).remove();
				_tabContLayer = _tabCont.children(opt.tabContLayer);
				if ( _this.children(opt.tabCurrent).length < 1) {
					
					_this.children(opt.tabElem).eq(0).trigger("click");
				}
				
			});
		});
	};
	$.fn.rsAccordion = function(options) {
		var opt = $.extend({
			toggleCln:"show",		//切换显示的样式名
			level_1:".sal",			//第一层的选择器
			level_1_title:"dt",		//第一层的标签头
			level_1_data:"dd"		//第一层的标签内容
		},options);
		var _self = $(this);
		var _levels = _self.children(opt.level_1);
		_self.on("click", opt.level_1_title,function(e){
			var _this = $(this);
			var _paret = _this.parent();
			var _isShow = _levels.filter(".show");
			if ( _isShow[0] != _paret[0]) {
				_isShow.removeClass(opt.toggleCln);
				_paret.addClass(opt.toggleCln);
			} else {
				_paret.removeClass(opt.toggleCln);
			}
		});
		return this;
	};
	//
	$.fn.cusSelect = function(options) {
		var opt = $.extend({
			defText:"请选择",
			wrapClass:"",
			afselect:null
		},options);
		var _IE = (function(){
			var v = 3, div = document.createElement('div'), all = div.getElementsByTagName('i');
			while (
				div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
				all[0]
			);
			return v > 4 ? v : false ;
		}());
		return this.each(function(){
			var _self = $(this);
			var smshCLS = "cusSelectShow";
			var smtx = ".cusSelectText";
			var smlt = ".cusSelectListWrap";
			var smlit = ".cusIthems";
			var selectText = _self.find(smtx);
			var selectList = _self.find(smlt);
			var innerText = _self.find(".innerText");
			var hideinput = _self.find("input");
			var doc = $(document);
			var val;
			
			function closeSelect() {
				$(".cusSelectWrap").removeClass(smshCLS);
				$(smlt).hide(0).removeAttr('style');
				doc.unbind("click.closeSelect");
			}
			function ulposition() {
				var uloffsetTop = selectList.offset().top;
				if ( uloffsetTop + selectList.height() > doc.height() ) {
					selectList.css({
						top:'auto',
						bottom:selectText.outerHeight()
					});
				}
			}
			function toggleSelect(_self) {
				var smglist = $(smlt);
				var smglistVist = smglist.filter(function(){
					if ($(this).is(":visible")){
						return this;
					}
				});
				if ( smglistVist[0] == selectList[0] ) {
					selectList.hide(0);
					smglistVist.parent().removeClass(smshCLS);
				} else {
					if(_IE == 6) {
						selectList.width(_self.outerWidth());
					}
					smglistVist.hide(0);
					smglistVist.parent().removeClass(smshCLS);
					_self.addClass(smshCLS);
					selectList.show(0);
					ulposition();
				}
			}
			
			if (opt.defText) {
				window.smgDefText = opt.defText;
				selectText.text(opt.defText);
			}
			selectText.on("click", function(e){
				e.stopPropagation();
				
				doc.bind("click.closeSelect", function() {
					closeSelect();
				});
				toggleSelect(_self);
			});
			_self.on("click", function(e) {
				e.stopPropagation();
			});
			selectList.on("click", smlit, function(e) {
				var _this = $(this);
				var _text = _this.text();
				val = _this.attr("val");
				innerText.text(_text);
				_this.addClass("act").siblings().removeClass("act");
				_self.attr("val", val);
				hideinput.val(val);
				if ( opt.afselect ) {
					opt.afselect(val);
				}
				closeSelect();
			});
		});
	};
	$.fn.cusSelectReset = function(options) {
		var opt = $.extend({
			defWrap:".cusSelectWrap",
			defText:".cusSelectText",
			defLWrp:".cusSelectListWrap"
		}, options);
		$(document).trigger("click");
		return this.each(function() {
			var _self = $(this);
			_self.removeAttr("val");
			_self.find(opt.defText).text(smgDefText);
		});
	};
})(jQuery);


function initLayout(){
	$(".rs_layout").rsLayout();
	$(".nav_tabwps_1").rs_simpleTabs({
		tabElem				:".nav_tapIthems",	//切换标签的子元素
		tabCurrent			:"cur",				//切换标签的Current状态
		tabCont				:".reTabcon",		//切换的元素内容父级
		tabContLayer		:".rTablayer",		//切换的元素内容层
		tabContLayerShow	:"show",			//切换的元素Current状态
		afclick				:null				//回调函数接口			
	});
	
	
	$(".rsmenTab").rs_simpleTabs({
		tabElem				:".rt_item",	//切换标签的子元素
		tabCurrent			:"cur",				//切换标签的Current状态
		tabCont				:".rsmenTabconwrap",		//切换的元素内容父级
		tabContLayer		:".rsmenTabcon",		//切换的元素内容层
		tabContLayerShow	:"show",			//切换的元素Current状态
		afclick				:null				//回调函数接口			
	});
	
	
	
	//自定义下拉
	$(".cusSelect_1").cusSelect({
		defText:""
	});
	
	//minHideTable
	var minColoumHide = $(".minIsHide");
	if (minColoumHide.length > 0 ) {
		var _aotoHideElemts;
		var _parent;
		var _index;
		minColoumHide.each(function() {
			var _autoHideElemt = $(this);
			_index = _autoHideElemt.index();
			_parent = _autoHideElemt.closest("table");
		});
		_parent.find("tr").each(function() {
			_aotoHideElemts = $.merge($(_aotoHideElemts), $(this).find("td").eq(_index));
		});
		_aotoHideElemts=$.merge(_aotoHideElemts, minColoumHide);
		
		$(window).on("ready resize", function() {
			if (_parent.width() <= "1000" ) {
				_aotoHideElemts.hide(0);
			} else {
				_aotoHideElemts.show(0);
			}
		});
	}
	//niceScroll
	treeNiceScroll = $(".lftTreeWrap, .tableScrollWrap,.con_wrap_con,.right_tab_con").niceScroll({cursorcolor:"#d7d7d7"});
	$(window).resize();
}
var layoutIniting = false ;
//DOM ready
$(function(){
	initLayout();
	layoutIniting = true ;
});

$(window).on('beforeunload',function() {   	
	//这里用于防止用户频繁刷新或者不小心关闭。
//	return "你确定关闭?";
	if(tipCloseWindow){
		return tipCloseWindowMessage;
	}
});

//20140128 paul
window.onload=window.onscroll=window.onresize=function(){
	var screenHerght=$(window).height(),artConHeight=$(".artCon").outerHeight(),bodyScrollTop=$("body").scrollTop(),artConWidth=$(".artCon").outerWidth();
	//console.log(screenHerght+" "+artConHeight+" "+bodyScrollTop)
	$(".artCon").css({"top":(screenHerght-artConHeight)/2+bodyScrollTop,"margin-left":-artConWidth/2});	
};

function artConShow(e){
	$(".screenBg").show();
	$(".artCon").eq(e).show();
}
function closeArt(){
	$(".screenBg,.artCon").hide();
}
$(document).ready( function (){
   //var h=$(window).height();// 浏览器当前窗口可视区域高
	//$(".designer_k").css({"height": h-300 +'px'});
	//$(".lf_ct").css({"height": h-183 +'px'});
});

//20140310 Bina
$(document).ready(function(){
	$(".shrinkage .sk_bt").addClass("active");
	$(".shrinkage table:not(:first)").hide();
 
//	$(".shrinkage .sk_bt").click(function(){
//		$(this).next("table").slideToggle("fast")
//		.siblings("table").slideUp("fast");
//		$(this).toggleClass("active");
//		$(this).siblings(".sk_bt").removeClass("active");
//	});
});


function openBottom(){
	if($('.rs_layout_bottom').hasClass("bottomminsize")){
		$(".rs_layout_bottom .minsize").click();
	}
}

function closeBottom(){
	if(!$('.rs_layout_bottom').hasClass("bottomminsize")){
		$(".rs_layout_bottom .minsize").click();
	}
}