package org.rivu.web.servlet;

import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.SystemLog;
import com.rivues.util.log.Logger;
import com.rivues.util.log.LoggerFactory;
import com.rivues.util.service.system.DataPersistenceService;

public class SystemInitServlet extends HttpServlet{
	private static Logger log = LoggerFactory.getLogger(SystemInitServlet.class) ;
	/**
	 * 
	 */
	private static final long serialVersionUID = -8466250302984592411L;
	/**
	 * 初始化完成
	 */
	public void init() throws ServletException {
		/**
		 * 标记启动成功
		 */
		RivuDataContext.setStart(true) ;
		
		log.info("系统初始化完成，系统初始化花费时间：" + (System.currentTimeMillis() - RivuDataContext.getStartTime().getTime())/1000 +" s", "SYSTEM", "SYSTEM") ;
		SystemLog systemLog = new SystemLog() ;
		systemLog.setOrgi("SYSTEM");
		systemLog.setStartid(RivuDataContext.getSystemStartID()) ;
		systemLog.setStarttime(RivuDataContext.getStartTime()) ;
		systemLog.setEndtime(new Date());
		systemLog.setTimes((int)(systemLog.getEndtime().getTime()-systemLog.getStarttime().getTime())) ;
		systemLog.setType(RivuDataContext.SystemLogType.START.toString());
		systemLog.setStatus("true") ;		//启动成功
		DataPersistenceService.persistence(systemLog) ;
 	}
}
