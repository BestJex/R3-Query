package com.rivues.module.report.web.handler.report;

import java.net.URLDecoder;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.rivues.core.RivuDataContext;
import com.rivues.module.account.web.handler.LoginController;
import com.rivues.module.platform.web.handler.ResponseData;
import com.rivues.module.platform.web.model.AnalyzerReport;
import com.rivues.module.platform.web.model.AnalyzerReportModel;
import com.rivues.module.platform.web.model.Cube;
import com.rivues.module.platform.web.model.CubeLevel;
import com.rivues.module.platform.web.model.CubeMeasure;
import com.rivues.module.platform.web.model.CubeMetadata;
import com.rivues.module.platform.web.model.Dimension;
import com.rivues.module.platform.web.model.PublishedCube;
import com.rivues.module.platform.web.model.ReportFilter;
import com.rivues.module.report.web.handler.ReportHandler;
import com.rivues.module.report.web.model.PublishedReport;
import com.rivues.util.RivuTools;
import com.rivues.util.data.ReportData;
import com.rivues.util.data.ValueData;
import com.rivues.util.iface.cube.CubeFactory;
import com.rivues.util.iface.report.R3Request;
import com.rivues.util.iface.report.ReportFactory;
import com.rivues.util.local.LocalTools;
import com.rivues.util.serialize.JSON;
import com.rivues.util.service.ServiceHelper;
import com.rivues.util.service.cache.CacheHelper;

@Controller  
@RequestMapping("/{orgi}/user/report") 
public class ReportViewController extends ReportHandler{
	
	/**
	 * 报表浏览页面
	 * @param request
	 * @param orgi
	 * @param location
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/view/{reportid}" , name="reportview" , type="user" , subtype="report")  
    public ModelAndView reportview(HttpServletRequest request , @PathVariable String orgi , @PathVariable String reportid , @Valid String vt , @Valid String ajax) throws Exception{  
    	ResponseData responseData = new ResponseData("/pages/user/report/view/reportview") ; 
    	ModelAndView view = null ;
    	PublishedReport report = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportid) ;
    	
    	if(report.getHtml()!=null && report.getHtml().equals("1")){
    		//允许匿名用户从移动端访问报表
    	}else if(super.getUser(request).getUsername().equals(RivuDataContext.GUEST_USER)){
    		return new LoginController().login(request, orgi) ;
    	}
    	
    	AnalyzerReportModel model = null ;
    	AnalyzerReport analyzerReport = null ;
    	if(report.getReportcontent()!=null && report.getReportcontent().length()>0){
    		/**
        	 * 
        	 */
    		analyzerReport = report.getReport() ;
    		if(analyzerReport.getModel().size()>0){
    			model = analyzerReport.getModel().get(0) ;
    		}
    	}
    	
    	if(vt!=null && vt.equals("lazy")){
    		/**
    		 * 延迟加载，先显示精度条，然后再加载数据
    		 */
    		responseData = new ResponseData("/pages/user/report/view/reportloadding")  ;
    		view = request(responseData , orgi) ;
    	}else{
    		if(ajax!=null && ajax.equals("true")){
    			responseData = new ResponseData("/pages/user/report/view/reportcontent")  ;
    		}
    		if(super.getQueryLog()!=null){
	    		super.getQueryLog().setDataid(report.getId()) ;
	    		super.getQueryLog().setFlowid(super.getFlowid()) ;
	    		super.getQueryLog().setDataname(report.getName()) ;
	    		super.getQueryLog().setDatatype(RivuDataContext.QueryLogEnum.REPORT.toString()) ;
	    		super.getQueryLog().setReportdic(report.getReportpackage()) ;
	    	}
    		if(model!=null){
    			
    			if(request.getParameter("sortType")!=null&&request.getParameter("sortName")!=null){
    				model.setSortName(request.getParameter("sortName"));
    				model.setSortType(request.getParameter("sortType"));
    				model.setSortstr(request.getParameter("sortType"));
        			PublishedCube publishedCube = CubeFactory.getCubeInstance().getCubeByID(model.getPublishedcubeid()) ;
        			Cube cube = publishedCube.getCube() ;
        				
        			for(Dimension dim : cube.getDimension()){
    					Iterator<CubeLevel> leveles = dim.getCubeLevel().iterator();

    					boolean found = false;
    					while (leveles.hasNext()) {
    						CubeLevel level = leveles.next();
    						if(level.getName().equals(model.getSortName())){
    							model.setSortstr(level.getCode());
    						}
    					}
    				}
        			for(CubeMeasure mea : cube.getMeasure()){
    					if(mea.getName().equals(model.getSortName())){
    						model.setSortstr(mea.getCode());
    					}
    					
    				}
        		}
    			
    			/**
    	    	 * 以下代码用于
    	    	 */
    	    	for(ReportFilter reportFilter : RivuTools.getRequestFilters(analyzerReport.getFilters())){
    	    		if(request.getParameter("drill")!=null){//如果是钻取，则清空所有过滤器的值
    	    			reportFilter.setRequeststartvalue(null);
    	    			reportFilter.setRequestendvalue(null);
    	    			reportFilter.setRequestvalue(null);
    	    		}
    	    		if(RivuDataContext.ReportCompareEnum.RANGE.toString().equals(reportFilter.getValuefiltertype())){
    	    			if(request.getParameter(reportFilter.getCode()+"_start")!=null && request.getParameter(reportFilter.getCode()+"_end")!=null){
    		    			if(RivuDataContext.FilterConValueEnum.AUTO.toString().equals(reportFilter.getConvalue())){
    		    				String start = request.getParameter(reportFilter.getCode()+"_start")!=null ? URLDecoder.decode(request.getParameter(reportFilter.getCode()+"_start")) : "" ;
    		    				String end = request.getParameter(reportFilter.getCode()+"_end")!=null ? URLDecoder.decode(request.getParameter(reportFilter.getCode()+"_end")) : "" ;
    		    				if((start.length() - start.replaceAll(".", "").length()) != (end.length() - end.replaceAll(".", "").length())){
    		    					continue ;
    		    				}
    		    			}
    		    			reportFilter.setRequeststartvalue(request.getParameter(reportFilter.getCode()+"_start")!=null?URLDecoder.decode(request.getParameter(reportFilter.getCode()+"_start")):request.getParameter(reportFilter.getCode()+"_start")) ;
    		    			reportFilter.setRequestendvalue(request.getParameter(reportFilter.getCode()+"_end")!=null?URLDecoder.decode(request.getParameter(reportFilter.getCode()+"_end")):request.getParameter(reportFilter.getCode()+"_end")) ;
    	    			}
    	    		}else{
    		    		if(request.getParameter(reportFilter.getCode())!=null){
    		    			reportFilter.setRequestvalue(request.getParameter(reportFilter.getCode())!=null?URLDecoder.decode(request.getParameter(reportFilter.getCode())):request.getParameter(reportFilter.getCode())) ;
    		    		}
    	    		}
    	    		
    	    		if(request.getParameter("drill")!=null&&!RivuDataContext.FilterConValueType.AUTO.toString().equals(reportFilter.getConvalue())
    	    				&&reportFilter.getRequestvalue()!=null
    	    				&&reportFilter.getRequestvalue().trim().startsWith("[")
    	    				&&reportFilter.getRequestvalue().trim().endsWith("]")){
    	    			reportFilter.setRequestvalue(reportFilter.getRequestvalue().substring(reportFilter.getRequestvalue().lastIndexOf("[")+1, reportFilter.getRequestvalue().lastIndexOf("]")));
    	    		}
    	    	}
        		/**
            	 * 更新缓存
            	 */
            	updateCacheReport(request , report , analyzerReport , orgi , false) ;
//    			model.setReportData(ServiceHelper.getReportService().service(request , model , analyzerReport , true , super.getQueryLog() , report.isCache()  , false)) ;
    		}
	    	view = request(responseData , orgi) ;
    	}
    	/**
    	 * 设置参数
    	 */
    	super.processRequestParam(analyzerReport, request , false) ;
    	view.addObject("loadata", true) ;
    	return "0".equals(report.getRolename()) ? super.createModelAndView(view, report, request,true)  : super.createReportView(view, report, request); 
    }
	
	/**
	 * 报表浏览页面
	 * @param request
	 * @param orgi
	 * @param location
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/mobile/{reportid}" , name="mobileview" , type="user" , subtype="report")  
    public ModelAndView mobileview(HttpServletRequest request , @PathVariable String orgi , @PathVariable String reportid , @Valid String vt , @Valid String ajax) throws Exception{  
    	ResponseData responseData = new ResponseData("/pages/user/report/mobile/reportview") ; 
    	ModelAndView view = null ;
    	PublishedReport report = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportid) ;
    	AnalyzerReportModel model = null ;
    	AnalyzerReport analyzerReport = null ;
    	if(report.getReportcontent()!=null && report.getReportcontent().length()>0){
    		/**
        	 * 
        	 */
    		analyzerReport = report.getReport() ;
    		if(analyzerReport.getModel().size()>0){
    			model = analyzerReport.getModel().get(0) ;
    		}
    	}
    	if(report.getHtml()!=null && report.getHtml().equals("1")){
    		//允许匿名用户从移动端访问报表
    	}else if(super.getUser(request).getUsername().equals(RivuDataContext.GUEST_USER)){
    		return new LoginController().login(request, orgi) ;
    	}
		if(super.getQueryLog()!=null){
    		super.getQueryLog().setDataid(report.getId()) ;
    		super.getQueryLog().setFlowid(super.getFlowid()) ;
    		super.getQueryLog().setDataname(report.getName()) ;
    		super.getQueryLog().setDatatype(RivuDataContext.QueryLogEnum.REPORT.toString()) ;
    		//super.getQueryLog().setReportdic(report.getReportpackage()) ;
    	}
		if(model!=null){
			/**
	    	 * 以下代码用于
	    	 */
	    	for(ReportFilter reportFilter : RivuTools.getRequestFilters(analyzerReport.getFilters())){
	    		if(request.getParameter("drill")!=null){//如果是钻取，则清空所有过滤器的值
	    			reportFilter.setRequeststartvalue(null);
	    			reportFilter.setRequestendvalue(null);
	    			reportFilter.setRequestvalue(null);
	    		}
	    		if(RivuDataContext.ReportCompareEnum.RANGE.toString().equals(reportFilter.getValuefiltertype())){
	    			if(request.getParameter(reportFilter.getCode()+"_start")!=null && request.getParameter(reportFilter.getCode()+"_end")!=null){
		    			if(RivuDataContext.FilterConValueEnum.AUTO.toString().equals(reportFilter.getConvalue())){
		    				String start = request.getParameter(reportFilter.getCode()+"_start")!=null ? URLDecoder.decode(request.getParameter(reportFilter.getCode()+"_start")) : "" ;
		    				String end = request.getParameter(reportFilter.getCode()+"_end")!=null ? URLDecoder.decode(request.getParameter(reportFilter.getCode()+"_end")) : "" ;
		    				if((start.length() - start.replaceAll(".", "").length()) != (end.length() - end.replaceAll(".", "").length())){
		    					continue ;
		    				}
		    			}
		    			reportFilter.setRequeststartvalue(request.getParameter(reportFilter.getCode()+"_start")!=null?URLDecoder.decode(request.getParameter(reportFilter.getCode()+"_start")):request.getParameter(reportFilter.getCode()+"_start")) ;
		    			reportFilter.setRequestendvalue(request.getParameter(reportFilter.getCode()+"_end")!=null?URLDecoder.decode(request.getParameter(reportFilter.getCode()+"_end")):request.getParameter(reportFilter.getCode()+"_end")) ;
	    			}
	    		}else{
		    		if(request.getParameter(reportFilter.getCode())!=null){
		    			reportFilter.setRequestvalue(request.getParameter(reportFilter.getCode())!=null?URLDecoder.decode(request.getParameter(reportFilter.getCode())):request.getParameter(reportFilter.getCode())) ;
		    		}
	    		}
	    		
	    		if(request.getParameter("drill")!=null&&!RivuDataContext.FilterConValueType.AUTO.toString().equals(reportFilter.getConvalue())
	    				&&reportFilter.getRequestvalue()!=null
	    				&&reportFilter.getRequestvalue().trim().startsWith("[")
	    				&&reportFilter.getRequestvalue().trim().endsWith("]")){
	    			reportFilter.setRequestvalue(reportFilter.getRequestvalue().substring(reportFilter.getRequestvalue().lastIndexOf("[")+1, reportFilter.getRequestvalue().lastIndexOf("]")));
	    		}
	    	}
		}
    	view = request(responseData , orgi) ;
    	/**
    	 * 设置参数
    	 */
    	super.processRequestParam(analyzerReport, request , false) ;
    	view.addObject("loadata", true) ;
    	return super.createModelAndView(view, report, request,true); 
    }
	
	/**
	 * 报表页面跳转，主要用于 快捷方式的跳转
	 * @param request
	 * @param orgi
	 * @param location
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/redirect/{reportid}" , name="reportview" , type="user" , subtype="report")  
    public ModelAndView reportRedirect(HttpServletRequest request , @PathVariable String orgi , @PathVariable String reportid) throws Exception{  
    	ResponseData responseData = new ResponseData("/pages/user/report/view/reportview") ; 
    	PublishedReport report = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportid) ;
    	if(report!=null && report.getTargetReport()!=null){
    		report = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, report.getTargetReport()) ;
    	}
    	
    	if(report.getExtparam()!=null && report.getExtparam().equals("true")){
			responseData = new ResponseData(new StringBuffer("redirect:/{orgi}/design/player/").append(report.getId()).append(".html").toString()) ;
		}else{
			responseData = new ResponseData(new StringBuffer("redirect:/{orgi}/user/report/view/").append(report.getId()).append(".html").toString()) ;
		}
    	return request(responseData , orgi); 
    }
	
	@RequestMapping(value = "/{reportid}/resetFilter", name="resetFilter" , type="design",subtype="custom")
   	public ModelAndView resetFilter(HttpServletRequest request , @PathVariable String  orgi ,  @PathVariable String  reportid) throws Exception{
       	ModelAndView view = request(super.createManageResponse("/pages/design/player/datadiv") , orgi) ;
       	PublishedReport report = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportid) ;
       	AnalyzerReport analyzerReport = report.getReport() ;
       	AnalyzerReportModel reportModel = null ;
       	if(analyzerReport.getModel().size()>0){
       		reportModel = analyzerReport.getModel().get(0);
       	}
       	if(reportModel != null){
   	    	/**
   	    	 * 以下代码用于
   	    	 */
   	    	for(ReportFilter reportFilter : analyzerReport.getFilters()){
   	    		if(RivuDataContext.ReportCompareEnum.RANGE.toString().equals(reportFilter.getValuefiltertype())){
   	    			reportFilter.setRequeststartvalue(null) ;
   	    			reportFilter.setRequestendvalue(null) ;
   	    		}else{
   		    		reportFilter.setRequestvalue(null) ;
   		    		
   	    		}
   	    	}
       	}
       	/**
       	 * 更新缓存
       	 */
       	updateCacheReport(request , report , analyzerReport , orgi , false) ;
       	
   		return this.reportview(request, orgi, reportid, null, null);
   	}
	
	
	 /**
     * 钻取数据
     * @param request
     * @param orgi
     * @param reportid
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/reportdrilldown/{reportid}/{modelid}/{pageNumber}/{row}/{col}" , name="reportdrilldown" , type="design",subtype="query")
    public ModelAndView reportdrilldown(HttpServletRequest request , @PathVariable String orgi, @PathVariable String reportid,@PathVariable String modelid,@PathVariable Integer pageNumber,@PathVariable Integer row,@PathVariable Integer col) throws Exception{
    	if(row==null||col==null||pageNumber==null){
    		new Exception(LocalTools.getMessage("E_REPORT_10010025"));
    	}
    	String p = request.getParameter("p");
    	R3Request req = new R3Request();
    	req.setParameters("p", pageNumber+"");
    	ModelAndView view = request(super.createManageResponse("/pages/user/report/view/reportview") , orgi) ;
    	PublishedReport report = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportid) ;
    	
    	AnalyzerReport analyzerReport = report.getReport() ;
    	AnalyzerReportModel reportModel = null ;
    	if(analyzerReport!=null && analyzerReport.getModel()!=null && analyzerReport.getModel().size()>0){
    		for(AnalyzerReportModel model : analyzerReport.getModel()){
    			if(modelid==null || modelid!=null && model.getId().equals(modelid)){
    				reportModel = model ;
    				break;
    			}
    		}
    	}
    	if(reportModel != null){
	    	/**
	    	 * 以下代码用于
	    	 */
	    	for(ReportFilter reportFilter : analyzerReport.getFilters()){
	    		if(RivuDataContext.ReportCompareEnum.RANGE.toString().equals(reportFilter.getValuefiltertype())){
	    			if(request.getParameter(reportFilter.getCode()+"_start")!=null && request.getParameter(reportFilter.getCode()+"_end")!=null){
		    			if(RivuDataContext.FilterConValueEnum.AUTO.toString().equals(reportFilter.getConvalue())){
		    				String start = request.getParameter(reportFilter.getCode()+"_start")!=null ? request.getParameter(reportFilter.getCode()+"_start") : "" ;
		    				String end = request.getParameter(reportFilter.getCode()+"_end")!=null ? request.getParameter(reportFilter.getCode()+"_end") : "" ;
		    				if((start.length() - start.replaceAll(".", "").length()) != (end.length() - end.replaceAll(".", "").length())){
		    					continue ;
		    				}
		    			}
		    			reportFilter.setRequeststartvalue(request.getParameter(reportFilter.getCode()+"_start")) ;
		    			reportFilter.setRequestendvalue(request.getParameter(reportFilter.getCode()+"_end")) ;
	    			}
	    		}else{
		    		if(request.getParameter(reportFilter.getCode())!=null){
		    			reportFilter.setRequestvalue(request.getParameter(reportFilter.getCode())) ;
		    		}
	    		}
	    	}
    	}
    	/**
    	 * 更新缓存
    	 */
    	updateCacheReport(request , report , analyzerReport , orgi , false) ;
    	/**
    	 * 创建视图
    	 */
    	/**
    	 * 将当期编辑的报表放入分布式缓存中，报表编辑器退出的时候需要清理该报表，同样的，在用户退出的时候，也需要清理缓存中的垃圾
    	 */
    	view.addObject("loadata", true) ;
    	view = createModelAndView(view , true , report , req , reportModel);
    	ReportData reportData = (ReportData) view.getModelMap().get("reportData") ;
    	if(reportData!=null && reportData.getData().size()>row && reportData.getData().get(row).size() > col){
    		
    		ValueData value = reportData.getData().get(row).get(col);
    	
    		PublishedCube publishedCube = CubeFactory.getCubeInstance().getCubeByID(reportModel.getPublishedcubeid()) ;
    		Cube modelcube = publishedCube.getCube() ;
    		modelcube.setDstype("table");
    		List<CubeMetadata> metas = modelcube.getMetadata();
    		
    		if(metas!=null&&metas.size()>0){
    			for(CubeMetadata tempMetadata : modelcube.getMetadata()) {
					if (value.getDrillthroughsql()!=null && ((tempMetadata.getMtype() == null)|| "0".equals(tempMetadata.getMtype())|| modelcube.getMetadata().size() == 1)) {
						String sql = value.getDrillthroughsql();
						StringBuffer sb = new StringBuffer(sql);
						for (String measurestr : reportModel.getMeasure().split(",")) {
							for (CubeMeasure measure : modelcube.getMeasure()) {
								if(measurestr.equals(measure.getId())&&!measure.isCalculatedmember()&&sql.indexOf(measure.getNameAlias().toUpperCase().toString())<0){//如果sql中不包含此sql那么就添加,如果是计算指标的话也不添加（数据抽取的时候是不存计算指标的）
//									sb.insert(sql.indexOf("from")-1, ", "+sql.substring(sql.indexOf("\"",sql.indexOf("from")), sql.indexOf("\"",sql.indexOf("\"",sql.indexOf("from"))+1)+1)+".\"MEA_"+measure.getNameAlias().toUpperCase()+"\" as \""+measure.getName()+"\"");
								
								}
							}
						}
						reportModel.setQuerytext(sb.toString());
					}
				}
    		}
    		request.setAttribute("drillflag", "drillflag");
    		view.addObject("row", row);
    		view.addObject("col", col);
    		view.addObject("pageNumber", pageNumber);
    		view.addObject("loadata", true) ;
    		view = createModelAndView(view , report ,modelcube, request , reportModel);
    		
    	}
    	
    	return view;
    }
    
	
	
	/**
     * 保存图表模式
     * @param request
     * @param orgi
     * @param reportid
     * @param modelid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "filterdrag/{reportid}/{modelid}", name="filterdrag" , type="design",subtype="query")
	public ModelAndView filterdrag(HttpServletRequest request , @PathVariable String  orgi , @PathVariable String  reportid,@PathVariable String modelid,@Valid ReportFilter reportFilter) throws Exception{
    	ModelAndView view = request(super.createManageResponse("/pages/user/report/view/loadfiltervalues") , orgi) ;
    	PublishedReport report = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportid) ;
    	AnalyzerReport analyzerReport = report.getReport() ;
    	AnalyzerReportModel reportModel = null ;
    	if(analyzerReport.getModel().size()>0){
    		for(AnalyzerReportModel model : analyzerReport.getModel()){
    			if(model.getId().equals(modelid)){
    				reportModel = model ;
    				break;
    			}
    		}
    	}
    	if(reportModel!=null){
    		for(ReportFilter rf : RivuTools.getRequestFilters(analyzerReport.getFilters())){
    			if(reportFilter.getId().equals(rf.getId())){
    				reportFilter = rf ;
    				StringBuffer strb = new StringBuffer() , temp = new StringBuffer();
    				if(request.getParameter("filter")!=null){
    					if(reportFilter.getDataname()!=null){
    						if(RivuDataContext.CubeEnum.CUBE.toString().equals(request.getParameter("type"))){
    							temp.append(request.getParameter("filter").replaceAll(",", "\\(-=-\\)"));
    						}else{
    							temp.append(request.getParameter("filter")) ;
    						}
    					}
    				}
    				boolean find = false ;
    				for(String ft : reportFilter.getDefaultvalue().split(RivuDataContext.DEFAULT_VALIUE_SPLIT)){
    					ft = ft.replaceAll("\\(-=-\\)", ",");
    					temp = new StringBuffer(temp.toString().replaceAll("\\(-=-\\)", ","));
    					if(strb.toString().indexOf(ft)<0){
    						if(!ft.equals(temp.toString()) || "true".equalsIgnoreCase(request.getParameter("selected"))){
	    						if(strb.length() >0){
	    							strb.append(RivuDataContext.DEFAULT_VALIUE_SPLIT) ;
	    						}
	    						strb.append(ft) ;
    						}
    					}
    					if(ft.equals(temp.toString())){
    						find = true ;
    					}
    				}
    				if(!find && "true".equalsIgnoreCase(request.getParameter("selected"))){
    					if(strb.length()>0){
    						strb.append(RivuDataContext.DEFAULT_VALIUE_SPLIT);
    					}
    					strb.append(temp.toString()) ;
    				}
    				reportFilter.setDefaultvalue(strb.toString().replaceAll("\\(-=-\\)", ",")) ;
    				view.addObject("filter", reportFilter);
    				break ;
    			}
    		}
    	}
    	/**
    	 * 更新缓存
    	 */
    	updateCacheReport(request , report , analyzerReport , orgi) ;
    	view.addObject("loadata", true) ;
    	return createModelAndView(view , false , report , request , reportModel);
	}
	
	/**
     * 保存图表模式
     * @param request
     * @param orgi
     * @param reportid
     * @param modelid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "filterdrag/{reportid}", name="reportviewfilterdrag" , type="design",subtype="query")
	public ModelAndView reportviewfilterdrag(HttpServletRequest request , @PathVariable String  orgi , @PathVariable String  reportid,@Valid ReportFilter reportFilter) throws Exception{
    	ModelAndView view = request(super.createManageResponse("/pages/public/success") , orgi) ;
    	PublishedReport report = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportid) ;
    	AnalyzerReport analyzerReport = report.getReport() ;
    	for(ReportFilter rf : analyzerReport.getFilters()){
			if(reportFilter.getDimid().equals(rf.getDimid())){
				reportFilter = rf ;
				StringBuffer strb = new StringBuffer() , temp = new StringBuffer();
				if(request.getParameter("filter")!=null){
					if(reportFilter.getDataname()!=null){
						if(RivuDataContext.CubeEnum.CUBE.toString().equals(request.getParameter("type"))){
							temp.append(request.getParameter("filter"));
						}else{
							temp.append(request.getParameter("filter")) ;
						}
					}
				}
				boolean find = false ;
				for(String ft : reportFilter.getDefaultvalue().split(RivuDataContext.DEFAULT_VALIUE_SPLIT)){
					if(strb.toString().indexOf(ft)<0){
						if(!ft.equals(temp.toString()) || "true".equalsIgnoreCase(request.getParameter("selected"))){
    						if(strb.length() >0){
    							strb.append(RivuDataContext.DEFAULT_VALIUE_SPLIT) ;
    						}
    						strb.append(ft) ;
						}
					}
					if(ft.equals(temp.toString())){
						find = true ;
					}
				}
				if(!find && "true".equalsIgnoreCase(request.getParameter("selected"))){
					if(strb.length()>0){
						strb.append(RivuDataContext.DEFAULT_VALIUE_SPLIT);
					}
					strb.append(temp.toString()) ;
				}
				reportFilter.setDefaultvalue(strb.toString()) ;
				break ;
			}
		}
    	/**
    	 * 更新缓存
    	 */
    	updateCacheReport(request , report , analyzerReport , orgi) ;
	
    	return view;
	}
}
