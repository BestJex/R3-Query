package com.rivues.module.report.web.model;

import com.rivues.module.platform.web.model.JobDetail;
import com.rivues.util.data.ValueData;

public interface EventTask extends java.io.Serializable{

	public String getTo();

	public String getTitle();

	public String getContent();
	
	public String getIsAppendix();
	
	public boolean process(TaskReportInfo taskReportInfo,JobDetail job) throws Exception ;
}