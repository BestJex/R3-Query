package com.rivues.module.report.web.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.JobDetail;
import com.rivues.util.RivuTools;

public class URLTask implements EventTask{
	private final Logger log = LoggerFactory.getLogger(URLTask.class); 
	/**
	 * 
	 */
	private static final long serialVersionUID = -1556212434060622351L;
	private String to ;
	private String title ;
	private String content ;
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public boolean process(TaskReportInfo taskReportInfo,JobDetail job) throws Exception {
		/**
		 * 发送邮件
		 */
		String url = RivuTools.processContent(this.content, taskReportInfo , false) ;
		/**
		 * 调用发送短信的接口， 短信接口的配置在 后台 系统管理->通知管理->短信网关
		 */
		log.info("URL TaskNode:"+url) ;
		RivuTools.getURL(url) ;
		return true;
	}
	@Override
	public String getIsAppendix() {
		// TODO Auto-generated method stub
		return null;
	}
}
