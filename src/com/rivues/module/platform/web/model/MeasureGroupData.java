package com.rivues.module.platform.web.model;

import java.io.Serializable;

public class MeasureGroupData  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String measureid;
	private String oldname;
	private String newname;
	public String getMeasureid() {
		return measureid;
	}
	public void setMeasureid(String measureid) {
		this.measureid = measureid;
	}
	public String getOldname() {
		return oldname;
	}
	public void setOldname(String oldname) {
		this.oldname = oldname;
	}
	public String getNewname() {
		return newname;
	}
	public void setNewname(String newname) {
		this.newname = newname;
	}

}
