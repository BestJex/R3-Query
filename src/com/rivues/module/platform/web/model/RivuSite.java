/**
 * Licensed to the Rivulet under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *     webapps/LICENSE-Rivulet
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rivues.module.platform.web.model;

import java.io.File;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author jaddy0302 Rivulet LocalFileTask.java 2010-3-1
 * 
 */
@Entity
@Table(name = "rivu5_site")
@org.hibernate.annotations.Proxy(lazy = false)
public class RivuSite implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 98876333686462570L;
	private String id ;
	private int defaultthreads = 2 ;
	private int defaulterrormsgnum = 20;
	private int operationmode = 2;  //Thread Mode ：2: Multithreading OR 1:Single-threaded
	private int maxthreads = 20;
	private int databasemaxthreads = 20 ;
	private int countthreads = 50 ; //All task max Thread ; 
	private int cacherec = 60000 ;
	private int cachememory = 50;  //Max Cache Memory /MB
	private int maxcachememory = 200 ; //All Task Max Memory 
	private String userid ;				//集群标志位，标识该节点属于哪个集群
	private String groupid ;			//服务器运行模式 server（单服务器） | distributed（分布式） | cluster（集群）
	private String searchindex = ".";
	private String uniquekey = "id" ;
	private int hlfragmenterlength = 200 ;
	private int maxrunningtask = 5 ;
	private String smtpserver ;   
	private String smtpuser;
	private String smtppassword ;
	private String mailfrom ;
	private String seclev;   //ssh...
	private String orgi;
	/**
	 * @return the id
	 */
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the defaultthreads
	 */
	public int getDefaultthreads() {
		return defaultthreads;
	}
	/**
	 * @param defaultthreads the defaultthreads to set
	 */
	public void setDefaultthreads(int defaultthreads) {
		this.defaultthreads = defaultthreads;
	}
	/**
	 * @return the defaulterrormsgnum
	 */
	public int getDefaulterrormsgnum() {
		return defaulterrormsgnum;
	}
	/**
	 * @param defaulterrormsgnum the defaulterrormsgnum to set
	 */
	public void setDefaulterrormsgnum(int defaulterrormsgnum) {
		this.defaulterrormsgnum = defaulterrormsgnum;
	}
	/**
	 * @return the operationmode
	 */
	public int getOperationmode() {
		return operationmode;
	}
	/**
	 * @param operationmode the operationmode to set
	 */
	public void setOperationmode(int operationmode) {
		this.operationmode = operationmode;
	}
	/**
	 * @return the maxthreads
	 */
	public int getMaxthreads() {
		return maxthreads;
	}
	/**
	 * @param maxthreads the maxthreads to set
	 */
	public void setMaxthreads(int maxthreads) {
		this.maxthreads = maxthreads;
	}
	/**
	 * @return the countthreads
	 */
	public int getCountthreads() {
		return countthreads;
	}
	/**
	 * @param countthreads the countthreads to set
	 */
	public void setCountthreads(int countthreads) {
		this.countthreads = countthreads;
	}
	/**
	 * @return the cacherec
	 */
	public int getCacherec() {
		return cacherec;
	}
	/**
	 * @param cacherec the cacherec to set
	 */
	public void setCacherec(int cacherec) {
		this.cacherec = cacherec;
	}
	/**
	 * @return the cachememory
	 */
	public int getCachememory() {
		return cachememory;
	}
	/**
	 * @param cachememory the cachememory to set
	 */
	public void setCachememory(int cachememory) {
		this.cachememory = cachememory;
	}
	/**
	 * @return the maxcachememory
	 */
	public int getMaxcachememory() {
		return maxcachememory;
	}
	/**
	 * @param maxcachememory the maxcachememory to set
	 */
	public void setMaxcachememory(int maxcachememory) {
		this.maxcachememory = maxcachememory;
	}
	/**
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}
	/**
	 * @param userid the userid to set
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}
	/**
	 * @return the groupid
	 */
	public String getGroupid() {
		return groupid;
	}
	/**
	 * @param groupid the groupid to set
	 */
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	/**
	 * @return the searchindex
	 */
	public String getSearchindex(String index) {
		return index.equalsIgnoreCase("search")?searchindex:"solr/"+index+"/data";
	}
	/**
	 * @return the searchindex
	 */
	public String getSearchindex() {
		return searchindex;
	}
	/**
	 * @param searchindex the searchindex to set
	 */
	public void setSearchindex(String searchindex) {
		this.searchindex = searchindex;
	}
	public String getUniquekey() {
		return uniquekey;
	}
	public void setUniquekey(String uniquekey) {
		this.uniquekey = uniquekey;
	}
	public int getHlfragmenterlength() {
		return hlfragmenterlength;
	}
	public void setHlfragmenterlength(int hlfragmenterlength) {
		this.hlfragmenterlength = hlfragmenterlength;
	}
	public int getMaxrunningtask() {
		return maxrunningtask;
	}
	public void setMaxrunningtask(int maxrunningtask) {
		this.maxrunningtask = maxrunningtask;
	}
	public int getDatabasemaxthreads() {
		return databasemaxthreads;
	}
	public void setDatabasemaxthreads(int databasemaxthreads) {
		this.databasemaxthreads = databasemaxthreads;
	}
	public String getSmtpserver() {
		return smtpserver;
	}
	public void setSmtpserver(String smtpserver) {
		this.smtpserver = smtpserver;
	}
	public String getSmtpuser() {
		return smtpuser;
	}
	public void setSmtpuser(String smtpuser) {
		this.smtpuser = smtpuser;
	}
	public String getSmtppassword() {
		return smtppassword;
	}
	public void setSmtppassword(String smtppassword) {
		this.smtppassword = smtppassword;
	}
	public String getMailfrom() {
		return mailfrom;
	}
	public void setMailfrom(String mailfrom) {
		this.mailfrom = mailfrom;
	}
	public String getSeclev() {
		return seclev;
	}
	public void setSeclev(String seclev) {
		this.seclev = seclev;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	
	
}
