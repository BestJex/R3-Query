package com.rivues.module.platform.web.model.warning;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.rivues.module.platform.web.model.MonitorBean;

@Entity
@Table(name = "rivu5_log_event")
@org.hibernate.annotations.Proxy(lazy = false)
public class EventBean extends MonitorBean{
	/**
	 * 
	 */
	private static final long serialVersionUID = -783517106258091730L;
	private String id ;
	private Date createdate = new Date();	//创建时间
	private int dataflag = (int)((System.currentTimeMillis()/1000)/5);		//时间标记
	private String hostname;	//服务器主机名
	private int port;		//服务器端口
	private String ipaddr ;		//服务器地址
	private String groupid ;	//改变用处，变更为  事件代码
	private String datatype;	//监控信息产生类型：auto
	private String triggerwarning ; //触发预警服务
	private String triggertime ;// 触发预警服务时间
	private String eventmsg ;	//事件消息
	private Date eventdate = new Date();	//事件时间
	private String eventype ;	//事件类型
	private String eventlevel;	//事件级别
	private String dataid ;		//报表对象ID
	private String dataname ;	//报表对象名称	
	private String userid ;		//用户ID
	private String username ;	//用户名称
	
	 
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public int getDataflag() {
		return dataflag;
	}
	public void setDataflag(int dataflag) {
		this.dataflag = dataflag;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getIpaddr() {
		return ipaddr;
	}
	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}
	public String getGroupid() {
		return groupid;
	}
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	public String getDatatype() {
		return datatype;
	}
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}
	public String getTriggerwarning() {
		return triggerwarning;
	}
	public void setTriggerwarning(String triggerwarning) {
		this.triggerwarning = triggerwarning;
	}
	public String getTriggertime() {
		return triggertime;
	}
	public void setTriggertime(String triggertime) {
		this.triggertime = triggertime;
	}
	public String getEventmsg() {
		return eventmsg;
	}
	public void setEventmsg(String eventmsg) {
		this.eventmsg = eventmsg;
	}
	public Date getEventdate() {
		return eventdate;
	}
	public void setEventdate(Date eventdate) {
		this.eventdate = eventdate;
	}
	public String getEventype() {
		return eventype;
	}
	public void setEventype(String eventype) {
		this.eventype = eventype;
	}
	public String getEventlevel() {
		return eventlevel;
	}
	public void setEventlevel(String eventlevel) {
		this.eventlevel = eventlevel;
	}
	public String getDataid() {
		return dataid;
	}
	public void setDataid(String dataid) {
		this.dataid = dataid;
	}
	public String getDataname() {
		return dataname;
	}
	public void setDataname(String dataname) {
		this.dataname = dataname;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
