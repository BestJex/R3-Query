package com.rivues.module.platform.web.model.chart;

import java.util.UUID;

import com.rivues.module.report.web.model.ModelPackage;

public class LayoutData implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2772752377448701291L;
	private String id = UUID.randomUUID().toString();
	private int rows ;
	private int cols ;
	private boolean head = false ;
	private String title ;
	private boolean display = true;

	private ModelPackage modelpkg ;
	private LayoutStyle style ;
	
	public LayoutData(){
		style = new LayoutStyle();
	}
	
	public LayoutData(ModelPackage modelpkg ){
		this.modelpkg = modelpkg ;
		style = new LayoutStyle();
	}
	public LayoutData(ModelPackage modelpkg , LayoutStyle style){
		this.modelpkg = modelpkg ;
		this.style = style ;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getCols() {
		return cols;
	}

	public void setCols(int cols) {
		this.cols = cols;
	}

	public ModelPackage getModelpkg() {
		return modelpkg;
	}
	public void setModelpkg(ModelPackage modelpkg) {
		this.modelpkg = modelpkg;
	}
	public LayoutStyle getStyle() {
		return style;
	}
	public void setStyle(LayoutStyle style) {
		this.style = style;
	}

	public boolean isDisplay() {
		return display;
	}

	public void setDisplay(boolean display) {
		this.display = display;
	}

	public boolean isHead() {
		return head;
	}

	public void setHead(boolean head) {
		this.head = head;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
