package com.rivues.module.platform.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "rivu5_selfreportchoicelevel")
@org.hibernate.annotations.Proxy(lazy = false)
public class SelfReportChoiceLevel implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private String id ;
	private String modelid;
	private String levelid;
	private String memberprefix;//过滤条件的前端信息  如 [时间].[年].[2012] 我们取 [时间].[年]. //如果是sql版本的  则为 数据库列名 creatname=
	private String choiceMembers;
	private String orgi;
	
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getModelid() {
		return modelid;
	}
	public void setModelid(String modelid) {
		this.modelid = modelid;
	}
	public String getLevelid() {
		return levelid;
	}
	public void setLevelid(String levelid) {
		this.levelid = levelid;
	}
	public String getChoiceMembers() {
		return choiceMembers;
	}
	public void setChoiceMembers(String choiceMembers) {
		this.choiceMembers = choiceMembers;
	}
	public String getMemberprefix() {
		return memberprefix;
	}
	public void setMemberprefix(String memberprefix) {
		this.memberprefix = memberprefix;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}

}
