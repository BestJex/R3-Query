package com.rivues.module.manage.web.handler.log;

import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.csvreader.CsvWriter;
import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.handler.Handler;
import com.rivues.module.platform.web.handler.RequestData;
import com.rivues.module.platform.web.handler.ResponseData;
import com.rivues.module.platform.web.model.Log;
import com.rivues.module.platform.web.model.QueryLog;
import com.rivues.module.platform.web.model.RequestLog;
import com.rivues.module.platform.web.model.SystemLog;
import com.rivues.util.RivuTools;
import com.rivues.util.log.Persistence;

@Controller  
@RequestMapping("/{orgi}/manage/log")  
public class LogController extends Handler{

	private final Logger log = LoggerFactory.getLogger(LogController.class); 
	
	
 
    
    @RequestMapping(value="/warnmonitor" , name="warnmonitor" , type="monitor",subtype="warn")
    public ModelAndView warnmonitor(HttpServletRequest request , @PathVariable String orgi, @ModelAttribute RequestData data) throws Exception{ 
    	ModelAndView view = request(super.createManageTempletResponse("/pages/manage/logmonitor") , orgi) ;
    	return view ;
    }
    
    /**
	 * 日志服务的默认页面，显示所有的 日志信息
	 * @param request
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value="/logmonitor" , name="logmonitor" , type="monitor",subtype="log")
    public ModelAndView logmonitor(HttpServletRequest request , @PathVariable String orgi, @ModelAttribute RequestData data) throws Exception{  
    	ModelAndView view = request(super.createManageTempletResponse("/pages/manage/log/index") , orgi) ; 
    	DetachedCriteria criteria = DetachedCriteria.forClass(Log.class).add(Restrictions.eq("orgi", "SYSTEM")).addOrder(Order.desc("createdate")) ;
    	if(request.getParameter("operationuser")!=null&&request.getParameter("operationuser").toString().length()>0){
    		criteria.add(Restrictions.like("username", request.getParameter("operationuser").toString(), MatchMode.ANYWHERE).ignoreCase());
    		view.addObject("operationuser", request.getParameter("operationuser").toString());
    	}
    	if(request.getParameter("usedtime")!=null&&request.getParameter("usedtime").toString().length()>0){
    		criteria.add(Restrictions.ge("triggertimes", Integer.parseInt(request.getParameter("usedtime").toString())));
    		view.addObject("usedtime", request.getParameter("usedtime").toString());
    	}
    	if(request.getParameter("searchkey")!=null&&request.getParameter("searchkey").toString().length()>0){
    		criteria.add(Restrictions.like("msg", request.getParameter("searchkey").toString(), MatchMode.ANYWHERE).ignoreCase());
    		view.addObject("searchkey", request.getParameter("searchkey").toString());
    	}
    	if(request.getParameter("beginTime")!=null&&request.getParameter("beginTime").toString().length()>0){
    		criteria.add(Restrictions.gt("createdate", RivuTools.stringToDate(request.getParameter("beginTime").toString())));
    		view.addObject("beginTime", request.getParameter("beginTime").toString());
    	}
    	if(request.getParameter("endTime")!=null&&request.getParameter("endTime").toString().length()>0){
    		criteria.add(Restrictions.le("createdate", RivuTools.stringToDate(request.getParameter("endTime").toString())));
    		view.addObject("endTime", request.getParameter("endTime").toString());
    	}
    	
    	if(request.getParameter("loglevel")!=null&&request.getParameter("loglevel").toString().length()>0){
    		criteria.add(Restrictions.eq("levels", request.getParameter("loglevel").toString()));
    		view.addObject("loglevel", request.getParameter("loglevel").toString());
    	}

    	view = page(request, orgi, view, criteria, "beginNum", "endNum", "loglist");
    	
    	
    	
    	return view ;
    }
    
    
    /**
	 * 日志服务的默认页面，显示所有的 日志信息
	 * @param request
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value="/reportoperation" , name="reportoperation" , type="monitor",subtype="log")
    public ModelAndView reportoperation(HttpServletRequest request , @PathVariable String orgi , @ModelAttribute RequestData data) throws Exception{  
    	ModelAndView view = request(super.createManageTempletResponse("/pages/manage/log/reportoperation") , orgi) ; 
    	DetachedCriteria criteria = DetachedCriteria.forClass(QueryLog.class).add(Restrictions.eq("orgi", orgi)).addOrder(Order.desc("createdate")) ;
		
		if(request.getParameter("operationuser")!=null&&request.getParameter("operationuser").toString().length()>0){
    		criteria.add(Restrictions.like("username", request.getParameter("operationuser").toString(), MatchMode.ANYWHERE).ignoreCase());
    		view.addObject("operationuser", request.getParameter("operationuser").toString());
    	}
		if(request.getParameter("operationtype")!=null&&request.getParameter("operationtype").toString().length()>0){
    		criteria.add(Restrictions.eq("name", request.getParameter("operationtype").toString()));
    		view.addObject("operationtype", request.getParameter("operationtype").toString());
    	}
    	if(request.getParameter("usedtime")!=null&&request.getParameter("usedtime").toString().length()>0){
    		criteria.add(Restrictions.ge("querytime", Long.parseLong(request.getParameter("usedtime").toString())*1000));
    		view.addObject("usedtime", request.getParameter("usedtime").toString());
    	}
    	if(request.getParameter("searchkey")!=null&&request.getParameter("searchkey").toString().length()>0){
    		criteria.add(Restrictions.like("reportdic", request.getParameter("searchkey").toString(), MatchMode.ANYWHERE).ignoreCase());
    		view.addObject("searchkey", request.getParameter("searchkey").toString());
    	}
    	if(request.getParameter("beginTime")!=null&&request.getParameter("beginTime").toString().length()>0){
    		criteria.add(Restrictions.gt("createdate", RivuTools.stringToDate(request.getParameter("beginTime").toString())));
    		view.addObject("beginTime", request.getParameter("beginTime").toString());
    	}
    	if(request.getParameter("endTime")!=null&&request.getParameter("endTime").toString().length()>0){
    		criteria.add(Restrictions.le("createdate", RivuTools.stringToDate(request.getParameter("endTime").toString())));
    		view.addObject("endTime", request.getParameter("endTime").toString());
    	}
		
		view = page(request, orgi, view, criteria, "beginNum", "endNum", "loglist");
    	
    	return view ;
    }
    
    /**
	 * 日志服务的默认页面，显示所有的 日志信息
	 * @param request
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value="/logbus/{type}" , name="logbus" , type="monitor",subtype="log")
    public ModelAndView logbus(HttpServletRequest request , @PathVariable String orgi , @PathVariable String type, @ModelAttribute RequestData data) throws Exception{  
    	ModelAndView view = request(super.createManageTempletResponse("/pages/manage/log/logbus") , orgi) ; 
    	//view.addObject("loglist", Persistence.getLogPersistence().getRequestLogList(orgi, type , 0, Handler.PAGE_SIZE_FV)) ;
    	DetachedCriteria criteria = DetachedCriteria.forClass(RequestLog.class).add(Restrictions.eq("orgi", orgi)).addOrder(Order.desc("starttime")) ;
    	String[] types; //存储多参数
		types=type.trim().split("-");  //参数按照'-'符号拆分
		
		if(types.length>1){   //若参数不唯一，则添加条件
		  if(types[0]!=null && types[1]!=null && types[0].length()>0 && types[1].length()>0){	
			criteria.add(Restrictions.eq("funtype", types[1])).add(Restrictions.eq("name", types[0]));
		  }	
		}else{
		
		  if(type!=null && type.length() > 0){
			criteria.add(Restrictions.eq("funtype", type)) ;
		  }
		
		}
		
		if(request.getParameter("operationuser")!=null&&request.getParameter("operationuser").toString().length()>0){
    		criteria.add(Restrictions.like("username", request.getParameter("operationuser").toString(), MatchMode.ANYWHERE).ignoreCase());
    		view.addObject("operationuser", request.getParameter("operationuser").toString());
    	}
    	if(request.getParameter("usedtime")!=null&&request.getParameter("usedtime").toString().length()>0){
    		criteria.add(Restrictions.ge("querytime", Long.parseLong(request.getParameter("usedtime").toString())*1000));
    		view.addObject("usedtime", request.getParameter("usedtime").toString());
    	}
    	if(request.getParameter("searchkey")!=null&&request.getParameter("searchkey").toString().length()>0){
    		criteria.add(Restrictions.like("url", request.getParameter("searchkey").toString(), MatchMode.ANYWHERE).ignoreCase());
    		view.addObject("searchkey", request.getParameter("searchkey").toString());
    	}
    	if(request.getParameter("beginTime")!=null&&request.getParameter("beginTime").toString().length()>0){
    		criteria.add(Restrictions.gt("createdate", RivuTools.stringToDate(request.getParameter("beginTime").toString())));
    		view.addObject("beginTime", request.getParameter("beginTime").toString());
    	}
    	if(request.getParameter("endTime")!=null&&request.getParameter("endTime").toString().length()>0){
    		criteria.add(Restrictions.le("createdate", RivuTools.stringToDate(request.getParameter("endTime").toString())));
    		view.addObject("endTime", request.getParameter("endTime").toString());
    	}
    	
//    	if(request.getParameter("loglevel")!=null&&request.getParameter("loglevel").toString().length()>0){
//    		criteria.add(Restrictions.eq("levels", request.getParameter("loglevel").toString()));
//    		view.addObject("loglevel", request.getParameter("loglevel").toString());
//    	}
		
		view = page(request, orgi, view, criteria, "beginNum", "endNum", "loglist");
    	
//    	if(type.indexOf("reportedito")>-1){
//    		type="authority";
//    	}
    	view.addObject("type", type) ;
    	return view ;
    }
    /**
     * 
     * @param request
     * @param orgi
     * @param type登陆登出
     * @param data
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/loguser/{type}" , name="loguser" , type="monitor",subtype="log")
    public ModelAndView loguser(HttpServletRequest request , @PathVariable String orgi , @PathVariable String type, @ModelAttribute RequestData data) throws Exception{  
    	ModelAndView view = request(super.createManageTempletResponse("/pages/manage/log/logcommonpage") , orgi) ; 
    	DetachedCriteria criteria = DetachedCriteria.forClass(Log.class).add(Restrictions.eq("levels", "INFO"));
    	view.addObject("loglist", Persistence.getLogPersistence().getBusLogPage(criteria, 0, Handler.PAGE_SIZE_FV)) ;
    	view.addObject("type", type) ;
    	return view ;
    }
    
    @RequestMapping(value="/logwarn/{type}" , name="logwarn" , type="monitor",subtype="log")
    public ModelAndView logwarn(HttpServletRequest request , @PathVariable String orgi , @PathVariable String type, @ModelAttribute RequestData data) throws Exception{  
    	ModelAndView view = request(super.createManageTempletResponse("/pages/manage/log/logcommonpage") , orgi) ; 
    	DetachedCriteria criteria = DetachedCriteria.forClass(Log.class).add(Restrictions.eq("levels", "WARN"));
    	//view.addObject("loglist", Persistence.getLogPersistence().getBusLogPage(criteria, 0, Handler.PAGE_SIZE_FV)) ;
    	view = page(request, orgi, view, criteria, "beginNum", "endNum", "loglist");
    	
    	view.addObject("type", type) ;
    	return view ;
    }
    
    @RequestMapping(value="/logerror/{type}" , name="logerror" , type="monitor",subtype="log")
    public ModelAndView logerror(HttpServletRequest request , @PathVariable String orgi , @PathVariable String type, @ModelAttribute RequestData data) throws Exception{  
    	ModelAndView view = request(super.createManageTempletResponse("/pages/manage/log/errorlogcommonpage") , orgi) ; 
    	DetachedCriteria criteria = DetachedCriteria.forClass(Log.class).add(Restrictions.eq("levels", "INFO"));
    	//view.addObject("loglist", Persistence.getLogPersistence().getBusLogPage(criteria, 0, Handler.PAGE_SIZE_FV)) ;
    	view = page(request, orgi, view, criteria, "beginNum", "endNum", "loglist");
    	
    	view.addObject("type", type) ;
    	return view ;
    }
    
    @RequestMapping(value="/logtacticsmgr/{type}" , name="logtacticsmgr" , type="monitor",subtype="log")
    public ModelAndView logtacticsmgr(HttpServletRequest request , @PathVariable String orgi , @PathVariable String type, @ModelAttribute RequestData data) throws Exception{  
    	ModelAndView view = request(super.createManageTempletResponse("/pages/manage/log/logcommonpage") , orgi) ; 
    	DetachedCriteria criteria = DetachedCriteria.forClass(Log.class).add(Restrictions.eq("levels", "INFO"));
    	view.addObject("loglist", Persistence.getLogPersistence().getBusLogPage(criteria, 0, Handler.PAGE_SIZE_FV)) ;
    	view.addObject("type", type) ;
    	return view ;
    }
    
    /**
	 * 日志服务的默认页面，显示所有的 租户日志信息
	 * @param request
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value="/log/syslog" , name="index" ,  type="monitor",subtype="log")
    public ModelAndView bus(HttpServletRequest request , @PathVariable String orgi, @ModelAttribute RequestData data) throws Exception{  
    	ModelAndView view = request(super.createManageTempletResponse("/pages/manage/log/systemlog") , orgi) ; 
    	//view.addObject("loglist", Persistence.getLogPersistence().getBusLogList(orgi, null , null , null , 0, Handler.PAGE_SIZE_FV)) ;
    	DetachedCriteria criteria = DetachedCriteria.forClass(Log.class).add(Restrictions.eq("orgi", orgi)).addOrder(Order.desc("createdate")) ;
  	  	
    	if(request.getParameter("operationuser")!=null&&request.getParameter("operationuser").toString().length()>0){
    		criteria.add(Restrictions.like("username", request.getParameter("operationuser").toString(), MatchMode.ANYWHERE).ignoreCase());
    		view.addObject("operationuser", request.getParameter("operationuser").toString());
    	}
    	if(request.getParameter("usedtime")!=null&&request.getParameter("usedtime").toString().length()>0){
    		criteria.add(Restrictions.ge("triggertimes", Integer.parseInt(request.getParameter("usedtime").toString())));
    		view.addObject("usedtime", request.getParameter("usedtime").toString());
    	}
    	if(request.getParameter("searchkey")!=null&&request.getParameter("searchkey").toString().length()>0){
    		criteria.add(Restrictions.like("msg", request.getParameter("searchkey").toString(), MatchMode.ANYWHERE).ignoreCase());
    		view.addObject("searchkey", request.getParameter("searchkey").toString());
    	}
    	if(request.getParameter("beginTime")!=null&&request.getParameter("beginTime").toString().length()>0){
    		criteria.add(Restrictions.gt("createdate", RivuTools.stringToDate(request.getParameter("beginTime").toString())));
    		view.addObject("beginTime", request.getParameter("beginTime").toString());
    	}
    	if(request.getParameter("endTime")!=null&&request.getParameter("endTime").toString().length()>0){
    		criteria.add(Restrictions.le("createdate", RivuTools.stringToDate(request.getParameter("endTime").toString())));
    		view.addObject("endTime", request.getParameter("endTime").toString());
    	}
    	
    	if(request.getParameter("loglevel")!=null&&request.getParameter("loglevel").toString().length()>0){
    		criteria.add(Restrictions.eq("levels", request.getParameter("loglevel").toString()));
    		view.addObject("loglevel", request.getParameter("loglevel").toString());
    	}
    	
    	view = page(request, orgi, view, criteria, "beginNum", "endNum", "loglist");
    	
    	//view.addObject("orgi", orgi);
    	
    	return view ;
    }
    
    @RequestMapping(value="/log/start" , name="index" , type="monitor",subtype="log")
    public ModelAndView systemLogStart(HttpServletRequest request , @PathVariable String orgi, @ModelAttribute RequestData data) throws Exception{  
    	ModelAndView view = request(super.createManageTempletResponse("/pages/manage/log/systemstart") , orgi) ; 
    	List<SystemLog> systemLogList = Persistence.getLogPersistence().getSystemLogList("SYSTEM", RivuDataContext.SystemLogType.START.toString() , 0, Handler.PAGE_SIZE_FV) ;
    	view.addObject("syslist", systemLogList) ;
    	if(systemLogList.size()>0){
    		
    		String logid = request.getParameter("logid");
    		if(logid==null){
    			SystemLog systemLog = systemLogList.get(0) ;
    			logid = systemLog.getStartid();
    		}
    		view.addObject("loglist", Persistence.getLogPersistence().getBusLogList("SYSTEM", null , null , logid , 0, Handler.PAGE_SIZE_FV)) ;
    		view.addObject("logid",logid);
    	}
    	return view ;
    }
    @RequestMapping(value="/log/stop" , name="index" , type="monitor",subtype="log")
    public ModelAndView systemLogStop(HttpServletRequest request , @PathVariable String orgi, @ModelAttribute RequestData data) throws Exception{  
    	ModelAndView view = request(super.createManageTempletResponse("/pages/manage/log/systemstop") , orgi) ; 
    	List<SystemLog> systemLogList = Persistence.getLogPersistence().getSystemLogList("SYSTEM", RivuDataContext.SystemLogType.STOP.toString() , 0, Handler.PAGE_SIZE_FV) ;
    	view.addObject("syslist", systemLogList) ;
    	if(systemLogList.size()>0){
    		String logid = request.getParameter("logid");
    		if(logid==null){
    			SystemLog systemLog = systemLogList.get(0) ;
    			logid = systemLog.getStartid();
    		}
    		view.addObject("loglist", Persistence.getLogPersistence().getBusLogList("SYSTEM", null , null , logid , 0, Handler.PAGE_SIZE_FV)) ;
    		view.addObject("logid",logid);
    	}
    	return view ;
    }
    
    @RequestMapping(value="/{errorid}/errormsgpannel" , name="index" , type="monitor",subtype="log")
    public ModelAndView errormsgpannel(HttpServletRequest request , @PathVariable String orgi,@PathVariable String errorid) throws Exception{  
    	ModelAndView view = request(new ResponseData("/pages/manage/log/errormsgpannel") , orgi) ; 
    	view.addObject("log",super.getService().getIObjectByPK(Log.class, errorid));
    	return view ;
    }
    
    @SuppressWarnings("unchecked")
	@RequestMapping(value="/log/exprotReportCSV", name="exprotReportCSV" , type="exprotReportCSV",subtype="log")
    public ModelAndView exprotReportCSV(HttpServletRequest request,HttpServletResponse response,@PathVariable String orgi) throws Exception{
    	ModelAndView view = request(super.createManageTempletResponse("/pages/manage/log/reportoperation") , orgi) ; 
    	DetachedCriteria criteria = DetachedCriteria.forClass(QueryLog.class).add(Restrictions.eq("orgi", orgi)).addOrder(Order.desc("createdate")) ;
		
		if(request.getParameter("operationuser")!=null&&request.getParameter("operationuser").toString().length()>0){
    		criteria.add(Restrictions.like("username", request.getParameter("operationuser").toString(), MatchMode.ANYWHERE).ignoreCase());
    		view.addObject("operationuser", request.getParameter("operationuser").toString());
    	}
		if(request.getParameter("operationtype")!=null&&request.getParameter("operationtype").toString().length()>0){
    		criteria.add(Restrictions.eq("name", request.getParameter("operationtype").toString()));
    		view.addObject("operationtype", request.getParameter("operationtype").toString());
    	}
    	if(request.getParameter("usedtime")!=null&&request.getParameter("usedtime").toString().length()>0){
    		criteria.add(Restrictions.ge("querytime", Long.parseLong(request.getParameter("usedtime").toString())*1000));
    		view.addObject("usedtime", request.getParameter("usedtime").toString());
    	}
    	if(request.getParameter("searchkey")!=null&&request.getParameter("searchkey").toString().length()>0){
    		criteria.add(Restrictions.like("reportdic", request.getParameter("searchkey").toString(), MatchMode.ANYWHERE).ignoreCase());
    		view.addObject("searchkey", request.getParameter("searchkey").toString());
    	}
    	if(request.getParameter("beginTime")!=null&&request.getParameter("beginTime").toString().length()>0){
    		criteria.add(Restrictions.gt("createdate", RivuTools.stringToDate(request.getParameter("beginTime").toString())));
    		view.addObject("beginTime", request.getParameter("beginTime").toString());
    	}
    	if(request.getParameter("endTime")!=null&&request.getParameter("endTime").toString().length()>0){
    		criteria.add(Restrictions.le("createdate", RivuTools.stringToDate(request.getParameter("endTime").toString())));
    		view.addObject("endTime", request.getParameter("endTime").toString());
    	}
    	
    	// 查询所有日志信息
    	List<QueryLog> logLst =super.getService().findAllByCriteria(criteria);
    	
    	// 生成提示信息，  
    	response.setContentType("charset=UTF-8;application/octet-stream");  
		String fileName = "R3_REPORT_LOG_"+System.currentTimeMillis()+".csv";
		fileName = URLEncoder.encode(fileName, "UTF-8");
        response.addHeader("Content-Disposition", "attachment;filename="+ fileName);
        
        OutputStream os = response.getOutputStream();
        CsvWriter wr = new CsvWriter(os, ',', Charset.forName("UTF-8"));
    	// 添加列标题
        wr.write("ID");
        wr.write("TYPE");
        wr.write("PARAMETERS");
        wr.write("THROWABLE");
        wr.write("USERNAME");
        wr.write("USERMAIL");
        wr.write("FILENAME");
        wr.write("ORGI");
        wr.write("ERROR");
        wr.write("DATAPROCESSTIME");
    	wr.write("DATAFORMATIME");
    	wr.write("QUERYPARSETIME");
    	wr.write("EXECUTETIME");
    	wr.write("CLASSNAME");
    	wr.write("STARTTIME");
    	wr.write("ENDTIME");
    	wr.write("DETAILTYPE");
    	wr.write("URL");
    	wr.write("REPORTDIC");
    	wr.write("DATAID");
    	wr.write("DATANAME");
    	wr.write("DATATYPE");
    	wr.write("REPORTNAME");
    	wr.write("IP");
    	wr.write("HOSTNAME");
    	wr.write("STATUES");
    	wr.write("METHODNAME");
    	wr.write("LINENUMBER");
    	wr.write("QUERYTIME");
    	wr.write("OPTEXT");
    	wr.write("FIELD6");
    	wr.write("FIELD7");
    	wr.write("FIELD8");
    	wr.write("FLOWID");
    	wr.write("USERID");
    	wr.write("NAME");
    	wr.write("FUNTYPE");
    	wr.write("FUNDESC");
    	wr.write("TRIGGERWARNING");
    	wr.write("TRIGGERTIME");
    	wr.write("CREATEDATE");
        wr.endRecord();
    	
    	if(logLst.size()>0){
        	// 循环给列值
        	for(QueryLog log : logLst){
        		wr.write(log.getId());
        		wr.write(log.getType());
        		wr.write(log.getParameters());
        		wr.write(log.getThrowable());
        		wr.write(log.getUsername());
        		wr.write(log.getUsermail());
        		wr.write(log.getFilename());
        		wr.write(log.getOrgi());
        		wr.write(log.getError());
        		wr.write(log.getDataprocesstime()+"");
        		wr.write(log.getDataformatime()+"");
        		wr.write(log.getQueryparsetime()+"");
        		wr.write(log.getExecutetime()+"");
        		wr.write(log.getClassname());
        		wr.write(RivuTools.dateToString(log.getStarttime(), "yyyy-MM-dd HH:mm:ss"));
        		wr.write(RivuTools.dateToString(log.getEndtime(), "yyyy-MM-dd HH:mm:ss"));
        		wr.write(log.getDetailtype());
        		wr.write(log.getUrl());
        		wr.write(log.getReportdic());
        		wr.write(log.getDataid());
        		wr.write(log.getDataname());
        		wr.write(log.getDatatype());
        		wr.write(log.getReportname());
        		wr.write(log.getIp());
        		wr.write(log.getHostname());
        		wr.write(log.getStatues());
        		wr.write(log.getMethodname());
        		wr.write(log.getLinenumber());
        		wr.write(log.getQuerytime()+"");
        		wr.write(log.getOptext());
        		wr.write(log.getField6());
        		wr.write(log.getField7());
        		wr.write(log.getField8());
        		wr.write(log.getFlowid());
        		wr.write(log.getUserid());
        		wr.write(log.getName());
        		wr.write(log.getFuntype());
        		wr.write(log.getFundesc());
        		wr.write(log.getTriggerwarning());
        		wr.write(log.getTriggertime());
        		wr.write(RivuTools.dateToString(log.getCreatedate(), "yyyy-MM-dd HH:mm:ss"));
        		wr.endRecord();
        		
        	}
    	}
    	wr.flush();
    	wr.close();
    	
//    	// 删除数据库中的已归档日志信息
//    	for(QueryLog log:logLst){
//    		super.getService().deleteIObject(log);
//    	}

    	return null;
    }
    
    @SuppressWarnings("unchecked")
	@RequestMapping(value="/log/exprotCSV", name="export" , type="export",subtype="log")
    public ModelAndView exprotCSV_Recode(HttpServletRequest request,HttpServletResponse response) throws Exception{
    	
    	// 获取条件查询对象
    	DetachedCriteria criteria  = DetachedCriteria.forClass(Log.class);
    	if(request.getParameter("operationuser")!=null&&request.getParameter("operationuser").toString().length()>0){
    		criteria.add(Restrictions.like("username", request.getParameter("operationuser").toString(), MatchMode.ANYWHERE).ignoreCase());
    	}
    	if(request.getParameter("usedtime")!=null&&request.getParameter("usedtime").toString().length()>0){
    		criteria.add(Restrictions.ge("triggertimes", Integer.parseInt(request.getParameter("usedtime").toString())));
    	}
    	if(request.getParameter("searchkey")!=null&&request.getParameter("searchkey").toString().length()>0){
    		criteria.add(Restrictions.like("msg", request.getParameter("searchkey").toString(), MatchMode.ANYWHERE).ignoreCase());
    	}
    	if(request.getParameter("beginTime")!=null&&request.getParameter("beginTime").toString().length()>0){
    		criteria.add(Restrictions.gt("createdate", RivuTools.stringToDate(request.getParameter("beginTime").toString())));
    	}
    	if(request.getParameter("endTime")!=null&&request.getParameter("endTime").toString().length()>0){
    		criteria.add(Restrictions.le("createdate", RivuTools.stringToDate(request.getParameter("endTime").toString())));
    	}
    	
    	if(request.getParameter("loglevel")!=null&&request.getParameter("loglevel").toString().length()>0){
    		criteria.add(Restrictions.eq("levels", request.getParameter("loglevel").toString()));
    	}
    	
    	// 查询所有日志信息
    	List<Log> logLst =super.getService().findAllByCriteria(criteria);
    	
    	// 生成提示信息，  
    	response.setContentType("charset=UTF-8;application/octet-stream");  
		String fileName = "R3_sys_log_"+System.currentTimeMillis()+".csv";
		fileName = URLEncoder.encode(fileName, "UTF-8");
        response.addHeader("Content-Disposition", "attachment;filename="+ fileName);
        
        OutputStream os = response.getOutputStream();
        CsvWriter wr = new CsvWriter(os, ',', Charset.forName("UTF-8"));
    	// 添加列标题
        wr.write("ID");
        wr.write("ORGI");
        wr.write("FLOWID");
        wr.write("LOGTYPE");
        wr.write("CREATEDATE");
        wr.write("MSG");
        wr.write("LEVELS");
        wr.write("THREAD");
        wr.write("CLAZZ");
        wr.write("FILES");
    	wr.write("LINENUMBER");
    	wr.write("METHOD");
    	wr.write("STARTID");
    	wr.write("ERRORINFO");
    	wr.write("TRIGGERWARNING");
    	wr.write("TRIGGERTIME");
    	wr.write("TRIGGERTIMES");
    	wr.write("NAME");
    	wr.write("CODE");
    	wr.write("MEMO");
    	wr.write("USERID");
    	wr.write("USERNAME");
    	wr.write("LOGTIME");
    	wr.write("IPADDR");
    	wr.write("PORT");
        wr.endRecord();
    	
    	if(logLst.size()>0){
        	// 循环给列值
        	for(Log log : logLst){
        		wr.write(log.getId());
        		wr.write(log.getOrgi());
        		wr.write(log.getFlowid());
        		wr.write(log.getLogtype());
        		wr.write( RivuTools.dateToString(log.getCreatedate(), "yyyy-MM-dd HH:mm:ss"));
        		wr.write(log.getMsg());
        		wr.write(log.getLevels());
        		wr.write(log.getThread());
        		wr.write(log.getClazz());
        		wr.write(log.getFiles());
        		wr.write(log.getLinenumber());
        		wr.write(log.getMethod());
        		wr.write(log.getStartid());
        		wr.write(log.getErrorinfo());
        		wr.write(log.getTriggerwarning());
        		wr.write(log.getTriggertime());
        		wr.write(Integer.toString(log.getTriggertimes()));
        		wr.endRecord();
        		
        	}
    	}
    	wr.flush();
    	wr.close();
    	
    	// 删除数据库中的已归档日志信息
//    	for(Log log:logLst){
//    		super.getService().deleteIObject(log);
//    	}

    	return null;
    }
    
    
	@RequestMapping(value="/log/{type}/operationLogExport", name="export" , type="export",subtype="log")
    public ModelAndView operationLogExport(HttpServletRequest request,HttpServletResponse response,@PathVariable String type,@PathVariable String orgi) throws Exception{
    	
    	// 获取条件查询对象
    	DetachedCriteria criteria = DetachedCriteria.forClass(RequestLog.class).add(Restrictions.eq("orgi", orgi)).addOrder(Order.desc("starttime")) ;
    	String[] types; //存储多参数
		types=type.trim().split("-");  //参数按照'-'符号拆分
		
		if(types.length>1){   //若参数不唯一，则添加条件
		  if(types[0]!=null && types[1]!=null && types[0].length()>0 && types[1].length()>0){	
			criteria.add(Restrictions.eq("funtype", types[1])).add(Restrictions.eq("name", types[0]));
		  }	
		}else{
		
		  if(type!=null && type.length() > 0){
			criteria.add(Restrictions.eq("funtype", type)) ;
		  }
		
		}
		
		if(request.getParameter("operationuser")!=null&&request.getParameter("operationuser").toString().length()>0){
    		criteria.add(Restrictions.like("username", request.getParameter("operationuser").toString(), MatchMode.ANYWHERE).ignoreCase());
    	}
    	if(request.getParameter("usedtime")!=null&&request.getParameter("usedtime").toString().length()>0){
    		criteria.add(Restrictions.ge("querytime", Long.parseLong(request.getParameter("usedtime").toString())*1000));
    	}
    	if(request.getParameter("searchkey")!=null&&request.getParameter("searchkey").toString().length()>0){
    		criteria.add(Restrictions.like("url", request.getParameter("searchkey").toString(), MatchMode.ANYWHERE).ignoreCase());
    	}
    	if(request.getParameter("beginTime")!=null&&request.getParameter("beginTime").toString().length()>0){
    		criteria.add(Restrictions.gt("createdate", RivuTools.stringToDate(request.getParameter("beginTime").toString())));
    	}
    	if(request.getParameter("endTime")!=null&&request.getParameter("endTime").toString().length()>0){
    		criteria.add(Restrictions.le("createdate", RivuTools.stringToDate(request.getParameter("endTime").toString())));
    	}
    	
    	
    	List<RequestLog> logLst =super.getService().findAllByCriteria(criteria);
    	
    	// 生成提示信息，  
    	response.setContentType("charset=UTF-8;application/octet-stream");  
		String fileName = "R3_operation_log_"+System.currentTimeMillis()+".csv";
		fileName = URLEncoder.encode(fileName, "UTF-8");
        response.addHeader("Content-Disposition", "attachment;filename="+ fileName);
        OutputStream os = response.getOutputStream();
        CsvWriter wr = new CsvWriter(os, ',', Charset.forName("UTF-8"));
        
     // 添加列标题
        wr.write("ID");
        wr.write("TYPE");
        wr.write("PARAMETERS");
        wr.write("THROWABLE");
        wr.write("USERNAME");
        wr.write("USERMAIL");
        wr.write("FILENAME");
        wr.write("ORGI");
        wr.write("ERROR");
        wr.write("CLASSNAME");
        wr.write("STARTTIME");
        wr.write("ENDTIME");
        wr.write("DETAILTYPE");
        wr.write("URL");
        wr.write("REPORTDIC");
        wr.write("REPORTNAME");
        wr.write("IP");
        wr.write("HOSTNAME");
        wr.write("STATUES");
        wr.write("METHODNAME");
        wr.write("LINENUMBER");
        wr.write("QUERYTIME");
        wr.write("OPTEXT");
        wr.write("FIELD6");
        wr.write("FIELD7");
        wr.write("FIELD8");
        wr.write("FLOWID");
        wr.write("USERID");
        wr.write("NAME");
        wr.write("FUNTYPE");
        wr.write("FUNDESC");
        wr.write("TRIGGERWARNING");
        wr.write("TRIGGERTIME");
        wr.write("CREATEDATE");
        
        wr.endRecord();
    	
    	if(logLst.size()>0){
        	// 循环给列值
        	for(RequestLog log : logLst){
        		
        		wr.write(log.getId());
        		wr.write(log.getType());
        		wr.write(log.getParameters());
        		wr.write(log.getThrowable());
        		wr.write(log.getUsername());
        		wr.write(log.getUsermail());
        		wr.write(log.getFilename());
        		wr.write(log.getOrgi());
        		wr.write(log.getError());
        		wr.write(log.getClassname());
        		wr.write(log.getStarttime().toString());
        		wr.write(log.getEndtime().toString());
        		wr.write(log.getDetailtype());
        		wr.write(log.getUrl());
        		wr.write(log.getReportdic());
        		wr.write(log.getReportname());
        		wr.write(log.getIp());
        		wr.write(log.getHostname());
        		wr.write(log.getStatues());
        		wr.write(log.getMethodname());
        		wr.write(log.getLinenumber());
        		wr.write(log.getQuerytime()+"");
        		wr.write(log.getOptext());
        		wr.write(log.getField6());
        		wr.write(log.getField7());
        		wr.write(log.getField8());
        		wr.write(log.getFlowid());
        		wr.write(log.getUserid());
        		wr.write(log.getName());
        		wr.write(log.getFuntype());
        		wr.write(log.getFundesc());
        		wr.write(log.getTriggerwarning());
        		wr.write(log.getTriggertime());
        		wr.write(log.getCreatedate().toString());
        		
        		wr.endRecord();
        		
        	}
    	}
    	wr.flush();
    	wr.close();
    	
    	// 删除数据库中的已归档日志信息
//    	for(RequestLog log:logLst){
//    		super.getService().deleteIObject(log);
//    	}

    	return null;
    }
    
    @RequestMapping(value="/{ip}/{port}/{service}" , name="databasepageservice" , type="monitor",subtype="database")
    public ModelAndView databasepageservice(HttpServletRequest request , HttpServletResponse response,@PathVariable String orgi,@PathVariable String service,@PathVariable String ip,@PathVariable String port , @ModelAttribute RequestData data) throws Exception{ 
    	ModelAndView view = request(new ResponseData("/pages/monitor/databasepageservice") , orgi) ;
    	int type = 2 ;
    	if(request.getParameter("sqlId")!=null){
    		view = request(new ResponseData("redirect:/{orgi}/om/{ip}/{port}/databasepage.html?service={service}&sqlId="+request.getParameter("sqlId")) , orgi) ;
    	}else{
    		view.addObject("json","{}");
    	}
    	return view ;
    }
  
}
