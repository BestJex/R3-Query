package com.rivues.module.manage.web.handler.task;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.rivu.handler.DCriteriaPageSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.rivues.module.platform.web.handler.Handler;
import com.rivues.module.platform.web.handler.RequestData;
import com.rivues.module.platform.web.handler.ResponseData;
import com.rivues.module.platform.web.model.ClusterServer;
import com.rivues.module.platform.web.model.ConfigureParam;
import com.rivues.module.platform.web.model.RivuSite;
import com.rivues.module.platform.web.model.SearchSetting;

@Controller
@RequestMapping("/{orgi}/manage/task/warn")
public class WarnController extends Handler {
	private final Logger log = LoggerFactory.getLogger(WarnController.class);
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value ="/searchsetting" , name ="searchsetting" , type ="manage", subtype ="task")
    public ModelAndView searchsetting(HttpServletRequest request , @PathVariable String orgi, @ModelAttribute RequestData data) throws Exception{
    	ModelAndView view = request(super.createManageTempletResponse("/pages/manage/task/searchsetting") , orgi) ;
    	List<SearchSetting> searchList = super.getService().findPageByCriteria(DetachedCriteria.forClass(SearchSetting.class)
    			.add(Restrictions.eq("orgi", orgi)));
    	DCriteriaPageSupport valueList = (DCriteriaPageSupport<?>)searchList;
		data.setTotal(valueList.getTotalCount());
		data.setPages(valueList.getIndexes().length) ;
		//获取到预警的所有非分隔的项
		List<ConfigureParam> propertiesList =
				super.getService().findAllByCriteria(
						DetachedCriteria.forClass(ConfigureParam.class)
								.add(Restrictions.eq("protype", "warn"))
								.add(Restrictions.not(Restrictions.eq("paramtype", "6")))
								.add(Restrictions.eq("orgi", orgi)));
		view.addObject("propertiesList",propertiesList);
		//获取到预警的所有根目录项
		List<ConfigureParam> datas = super.getService().findAllByCriteria(
				DetachedCriteria.forClass(ConfigureParam.class)
				.add(Restrictions.eq("protype", "warn"))
				.add(Restrictions.eq("paramtype", "6"))
				.add(Restrictions.or(Restrictions.isNull("groupid"),Restrictions.eq("groupid","")))
				.add(Restrictions.eq("orgi", orgi)));
		
		if(datas != null && datas.size() >0){
			view.addObject("datas",datas);
		}
    	view.addObject("searchList",searchList);
    	view.addObject("data",data);
		return view; 
    }

	@RequestMapping(value="/warnsettingadd", name="warnsettingadd", type="manage", subtype="task")
	public ModelAndView warnsettingadd(HttpServletRequest request,
			@PathVariable String orgi) throws Exception {
		ResponseData responseData = new ResponseData("/pages/manage/task/warnsettingadd") ; 
		ModelAndView view = request(responseData , orgi);
		//获取到预警的所有非分隔的项
		view.addObject("propertiesList",
				super.getService().findAllByCriteria(
						DetachedCriteria.forClass(ConfigureParam.class)
								.add(Restrictions.eq("protype", "warn"))
								.add(Restrictions.not(Restrictions.eq("paramtype", "6")))
								.add(Restrictions.eq("orgi", orgi))));
		//获取到预警的所有根目录项
		List<ConfigureParam> datas = super.getService().findAllByCriteria(
				DetachedCriteria.forClass(ConfigureParam.class)
				.add(Restrictions.eq("protype", "warn"))
				.add(Restrictions.eq("paramtype", "6"))
				.add(Restrictions.or(Restrictions.isNull("groupid"),Restrictions.eq("groupid","")))
				.add(Restrictions.eq("orgi", orgi)));
		
		if(datas != null && datas.size() >0){
			view.addObject("datas",datas);
		}
		return view;
	}
	
	@RequestMapping(value ="/searchsettingadd/{name}" ,name ="searchsettingadd" ,type ="manage" , subtype = "task")
    public ModelAndView searchsettingadd(HttpServletRequest request , @PathVariable String orgi, @PathVariable String name) throws Exception{
    	ResponseData responseData = new ResponseData("/pages/manage/task/searchsettingadd") ; 
    	List<SearchSetting> searchs = super.getService().findAllByCriteria(DetachedCriteria.forClass(SearchSetting.class)
    			.add(Restrictions.eq("orgi", orgi))
    			.add(Restrictions.eq("sitename", name)));
    	ModelAndView view = request(responseData , orgi);
    	if (searchs != null && searchs.size() >0){
    		view.addObject("search",searchs.get(0));
    	}else{
    		view.addObject("search",new SearchSetting());
    	}
    	//查询是否有设置邮件和短信
    	view.addObject(
				"site",
				super.getService().findAllByCriteria(
						DetachedCriteria.forClass(RivuSite.class).add(
								Restrictions.eq("orgi", orgi))));
    	//查询当前要设置预警的系统设置
		List<ConfigureParam> properties = super.getService().findAllByCriteria(DetachedCriteria.forClass(ConfigureParam.class)
													.add(Restrictions.eq("name", name))
													.add(Restrictions.eq("orgi", orgi)));
		//查询到服务器列表
		DetachedCriteria criteria = DetachedCriteria.forClass(ClusterServer.class).add(Restrictions.eq("orgi",orgi));
    	view.addObject("clusterList", super.getService().findAllByCriteria(criteria)) ;
				
		if(properties != null && properties.size() >0){
			view.addObject("properties",properties.get(0));
		}else{
			view.addObject("properties",new ConfigureParam());
		}
		return view;
    }
    
	@RequestMapping(value ="/searchsettingaddof" ,name ="searchsettingaddof" ,type ="manage" , subtype = "task")
    public ModelAndView searchsettingaddof(HttpServletRequest request , @PathVariable String orgi,@Valid SearchSetting searchSetting) throws Exception{
    	ResponseData response = new ResponseData(
				"redirect:/{orgi}/manage/task/warn/searchsetting.html");
    	int count = super.getService().getCountByCriteria(
    			DetachedCriteria.forClass(SearchSetting.class)
    			.add(Restrictions.eq("orgi", orgi))
    			.add(Restrictions.eq("sitename", searchSetting.getSitename())));
    	if(count > 0){
    		//已经创建
    		List<SearchSetting> searchs = super.getService().findAllByCriteria(
        			DetachedCriteria.forClass(SearchSetting.class).add(Restrictions.eq("orgi", orgi))
        			.add(Restrictions.eq("sitename", searchSetting.getSitename())));
    		SearchSetting setting = new SearchSetting();
        	if (searchs != null && searchs.size() >0){
        		setting = searchs.get(0);
        		setting.setHlhtmlend(searchSetting.getHlhtmlend());
        		setting.setHlhtmlstart(searchSetting.getHlhtmlstart());
        		setting.setHlsnippets(searchSetting.getHlsnippets());
        		setting.setSitedesc(searchSetting.getSitedesc());
        		setting.setStartdelay(searchSetting.getStartdelay());
        		setting.setR3cloudserver(searchSetting.getR3cloudserver());
        		if(searchSetting.getSiteemail() == null){
        			setting.setSiteemail("msg");
        		}else{
        			setting.setSiteemail(searchSetting.getSiteemail());
        		}
        		setting.setSitename(searchSetting.getSitename());
        		setting.setSearchtype(searchSetting.getSearchtype());
        		super.getService().updateIObject(setting);
        	}
    		
    	}else{
    		if(searchSetting.getSiteemail() == null){
    			searchSetting.setSiteemail("msg");
    		}
    		searchSetting.setSpellcheck("1");
    		//保存预警信息
    		super.getService().saveIObject(searchSetting);
    	}
    	return super.request(response, orgi);
    }
	
	@RequestMapping(value ="/searchsettingaddo" ,name ="searchsettingaddo" ,type ="manage" , subtype = "task")
    public ModelAndView searchsettingaddo(HttpServletRequest request , @PathVariable String orgi,@Valid String type,@Valid String value) throws Exception{
		ModelAndView view = request(
				super.createPageResponse("/pages/public/success"), orgi);
    	int count = super.getService().getCountByCriteria(
    			DetachedCriteria.forClass(SearchSetting.class)
    			.add(Restrictions.eq("orgi", orgi))
    			.add(Restrictions.eq("sitename", value)));
    	if(count > 0){
    		//已经创建
    		List<SearchSetting> searchs = super.getService().findAllByCriteria(
        			DetachedCriteria.forClass(SearchSetting.class).add(Restrictions.eq("orgi", orgi))
        			.add(Restrictions.eq("sitename", value)));
    		SearchSetting setting = new SearchSetting();
        	if (searchs != null && searchs.size() >0){
        		setting = searchs.get(0);
        		setting.setSpellcheck(type);
        		super.getService().updateIObject(setting);
        	}
    	}else{
    		SearchSetting setting = new SearchSetting();
    		setting.setSpellcheck(type);
    		setting.setSitename(value);
    		setting.setOrgi(orgi);
    		super.getService().saveIObject(setting);
    	}
    	return view;
    }

}
