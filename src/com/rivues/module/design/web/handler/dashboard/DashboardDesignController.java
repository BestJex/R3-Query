package com.rivues.module.design.web.handler.dashboard;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.rivues.core.RivuDataContext;
import com.rivues.core.RivuDataContext.ReportTypeEnum;
import com.rivues.module.design.web.handler.analytics.AnalyticsController;
import com.rivues.module.platform.web.handler.Handler;
import com.rivues.module.platform.web.handler.ResponseData;
import com.rivues.module.platform.web.interceptor.LogIntercreptorHandler;
import com.rivues.module.platform.web.model.AnalyzerReport;
import com.rivues.module.platform.web.model.AnalyzerReportModel;
import com.rivues.module.platform.web.model.Cube;
import com.rivues.module.platform.web.model.DataDic;
import com.rivues.module.platform.web.model.PublishedCube;
import com.rivues.module.platform.web.model.ReportFilter;
import com.rivues.module.report.web.handler.ReportHandler;
import com.rivues.module.report.web.model.PublishedReport;
import com.rivues.util.exception.DicExistEception;
import com.rivues.util.exception.DicNotExistException;
import com.rivues.util.exception.ReportException;
import com.rivues.util.iface.cube.CubeFactory;
import com.rivues.util.iface.report.ReportFactory;
import com.rivues.util.local.LocalTools;
import com.rivues.util.serialize.JSON;
import com.rivues.util.service.cache.CacheHelper;
  
@Controller  
@RequestMapping("/{orgi}/design/dashboard")  
public class DashboardDesignController  extends ReportHandler{  
	private final Logger log = LoggerFactory.getLogger(LogIntercreptorHandler.class); 
    /**
     * 获取报表，使用设计器打开
     * @param request
     * @param orgi
     * @param reportid
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/{reportid}" , name="dashboarddesign" , type="design",subtype="bashboard")
    public ModelAndView analyticsdesign(HttpServletRequest request , @PathVariable String orgi, @PathVariable String reportid) throws Exception{
    	return new AnalyticsController().analyticsdesign(request, orgi, reportid);
    }
    
    /**
	 * 报表新增页面
	 * @param request
	 * @param orgi
	 * @param location
	 * @return
	 */
	@RequestMapping(value="/{reportid}/reportadd" , name="reportadd" , type="user" , subtype="bashboard")  
    public ModelAndView reportadd(HttpServletRequest request , @PathVariable String orgi , @PathVariable String reportid)throws DicNotExistException{  
		
    	ResponseData responseData = new ResponseData("/pages/design/dashboard/reportadd") ; 
    	ModelAndView modelView = request(responseData , orgi) ;
    	modelView.addObject("reportid",reportid);
    	return modelView ; 
    }
	/**
	 * 
	 * @param request
	 * @param orgi
	 * @param location
	 * @param report
	 * @return
	 * @throws ReportException 
	 * @throws DicExistEception 
	 * @throws DicNotExistException 
	 */
	@RequestMapping(value="/{reportid}/reportaddo" , name="reportaddo" , type="user" , subtype="bashboard")  
    public ModelAndView reportaddo(HttpServletRequest request , @PathVariable String orgi, @PathVariable String reportid , @Valid PublishedReport report) throws ReportException, DicExistEception, DicNotExistException{  
		ResponseData responseData = new ResponseData("redirect:/{orgi}/design/dashboard/{reportid}.html?msgcode=S_REPORT_10010005") ;
		int count = ReportFactory.getReportInstance().getCount(DetachedCriteria.forClass(PublishedReport.class)
				.add(Restrictions.eq("orgi",orgi))
				.add(Restrictions.eq("name",report.getName()))
				.add(Restrictions.eq("reporttype",RivuDataContext.ReportTypeEnum.REPORT.toString()))
				.add(Restrictions.eq("dicid",report.getDicid())),super.getUser(request));
		if(count>0){
			responseData = new ResponseData("redirect:/{orgi}/design/dashboard/{reportid}.html?msgcode=E_REPORT_10010005") ;
		}else{
			DataDic dashboard = ReportFactory.getReportInstance().getReportDic(reportid, orgi, super.getUser(request)) ;
			report.setOrgi(orgi) ;
			report.setCreater(super.getUser(request).getId());
			report.setCreatetime(new Date()) ;
			report.setUpdatetime(new Date()) ;
			report.setCache(false);
			report.setUserid(dashboard.getId()) ;
			report.setBlacklist(RivuDataContext.ReportTypeEnum.DASHBOARD.toString()) ;
			report.setTabtype(dashboard.getTabtype()) ;
			report.setUsername(super.getUser(request).getUsername()) ;
			report.setPublishedtype(RivuDataContext.ReportStatusEnum.PUBLISHED.toString()) ;
			report.setUseremail(super.getUser(request).getEmail());
			report.setRolename(RivuDataContext.ReportTypeEnum.CUSREPORT.getType().toString());
			report.setReporttype(RivuDataContext.ReportTypeEnum.REPORT.toString()) ;
//			report.setPublishedtype(RivuDataContext.ReportStatusEnum.NEW.toString()) ;
			report.setStatus(RivuDataContext.ReportStatusEnum.AVAILABLE.toString()) ;
			report.setDicid(reportid) ;
			ReportFactory.getReportInstance().createReport(report , super.getUser(request)) ;
		}
		
		return request(new ResponseData("redirect:/{orgi}/design/dashboard/"+(report!=null && report.getId()!=null ? report.getId() : "{reportid}")+".html?msgcode=S_REPORT_10010005") , orgi) ; 
    }
	
	/**
	 * 
	 * @param request
	 * @param orgi
	 * @param location
	 * @param report
	 * @return
	 * @throws ReportException 
	 * @throws DicExistEception 
	 * @throws DicNotExistException 
	 */
	@RequestMapping(value="/{reportid}/delete" , name="delete" , type="user" , subtype="bashboard")  
    public ModelAndView delete(HttpServletRequest request , @PathVariable String orgi, @PathVariable String reportid) throws Exception{  
		
		PublishedReport report = ReportFactory.getReportInstance().getReportByID(super.getUser(request), orgi, reportid) ;
		if(report!=null){
			ReportFactory.getReportInstance().rmReportByID(reportid, super.getUser(request), orgi);
		}
		return request(new ResponseData("redirect:/{orgi}/design/dashboard/"+report.getDicid()+".html?msgcode=S_REPORT_10010008") , orgi) ;
		
    }
} 