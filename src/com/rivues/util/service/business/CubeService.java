package com.rivues.util.service.business;

import org.springframework.stereotype.Service;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.Cube;
import com.rivues.util.datasource.CubeTools;
import com.rivues.util.datasource.DataSource;
import com.rivues.util.service.monitor.BusinessService;

@Service("cube")
public class CubeService extends BusinessService{

	@Override
	public String getName() {
		return RivuDataContext.ServiceTypeName.CUBE.toString();
	}

	@Override
	public void service() throws Exception {
		
	}
	
	/**
	 * 
	 * @param cube
	 * @param database
	 * @return
	 */
	public DataSource service(Cube cube){
		return  new CubeTools(super.getDatabase(cube.getDb()));
	}

}
