package com.rivues.util.service.cache.impl.ehcache;

import com.rivues.util.service.cache.CacheBean;
import com.rivues.util.service.cache.impl.CacheInstance;
/**
 * EHCACHE缓存处理实例类
 * @author admin
 *
 */
public class EhCacheHelper implements CacheInstance{
	private static CacheBean distributedCacheBean = getDistributeCacheService() ;
	private static CacheBean distributedDictionaryCacheBean = getDistributeDictionaryService() ;
	private static CacheBean distributedReportDataCacheBean = getDistributeReportDataService() ;
	private static CacheBean sessionCacheBean = getDistributeSessionService() ;
	private static CacheBean mdxAggCacheBean = getMdxAggService() ;
	private static CacheBean clusterServerCacheBean = getClusterServerService() ;
	/**
	 * 服务类型枚举
	 * @author admin
	 *
	 */
	public enum CacheServiceEnum{
		EHCACHE_CACHE , EHCACHE_REPORTDATA_CACHE, EHCACHE_MAPCACHE, EHCACHE_DICTIONARY_CACHE , EHCACHE_SESSION_CACHE , MDX_AGG_CACHE , HAZLCAST_CLUSTER_SERVER_CACHE;
		public String toString(){
			return super.toString().toLowerCase();
		}
	}
	/**
	 * 获取报表对象服务
	 * @return
	 */
	private static CacheBean getDistributeCacheService(){
		return new EhCacheInstance().getCacheInstance(CacheServiceEnum.EHCACHE_MAPCACHE.toString());
	}
	/**
	 * 获取报表数据服务 ， 数据的默认有效期为 30分钟
	 * @return
	 */
	private static CacheBean getDistributeReportDataService(){
		return new EhCacheInstance().getCacheInstance(CacheServiceEnum.EHCACHE_REPORTDATA_CACHE.toString());
	}
	/**
	 * 获取字典数据服务
	 * @return
	 */
	private static CacheBean getDistributeDictionaryService(){
		return new EhCacheInstance().getCacheInstance(CacheServiceEnum.EHCACHE_DICTIONARY_CACHE.toString());
	}
	
	/**
	 * 获取字典数据服务
	 * @return
	 */
	private static CacheBean getDistributeSessionService(){
		return new EhCacheInstance().getCacheInstance(CacheServiceEnum.EHCACHE_SESSION_CACHE.toString());
	}
	
	/**
	 * 获取字典数据服务
	 * @return
	 */
	private static CacheBean getMdxAggService(){
		return new EhCacheInstance().getCacheInstance(CacheServiceEnum.MDX_AGG_CACHE.toString());
	}

	/**
	 * 获取服务器状态
	 * @return
	 */
	private static CacheBean getClusterServerService(){
		return new EhCacheInstance().getCacheInstance(CacheServiceEnum.HAZLCAST_CLUSTER_SERVER_CACHE.toString()) ;
	}
	
	/**
	 * 
	 * @return
	 */
	public CacheBean getDistributedCacheBean(){
		return distributedCacheBean;
	}
	
	/**
	 * 
	 * @return
	 */
	public CacheBean getDistributedDictionaryCacheBean(){
		return distributedDictionaryCacheBean ;
	}
	
	/**
	 * 
	 * @return
	 */
	public CacheBean getDistributedReporyDataCacheBean(){
		return distributedReportDataCacheBean;
	}

	/**
	 * 
	 * @return
	 */
	public CacheBean getDistributedSessionCacheBean(){
		return sessionCacheBean;
	}
	@Override
	public CacheBean getMDXAggCacheBean() {
		// TODO Auto-generated method stub
		return mdxAggCacheBean;
	}
	@Override
	public CacheBean getClusterServerCacheBean() {
		// TODO Auto-generated method stub
		return clusterServerCacheBean;
	}
}
