package com.rivues.util.service.system;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.warning.SystemInfoBean;
import com.rivues.util.service.monitor.BusinessService;
import com.rivues.util.tools.RuntimeData;

/**
 * 系统监控任务，定时执行，执行频率为每5秒一次
 * @author admin
 *
 */
@Component
public class SystemMonitorService extends BusinessService{
	
	private SystemInfoBean infobean = null ;	//最后一次执行的监控记录，用于计算网卡速率
	private static SystemInfoBean lastInfoBean ;	//上次启动服务器以来最后一次记录
	private static SystemInfoBean firstInfoBean ;	//本次服务器启动以来第一次记录
//	private static Sigar sigar = new Sigar();   	//静态对象，获取服务器信息
	private static boolean taskRuninit = false ;
	
	/**
	 * 系统监控任务，同时在该任务中执行预警，预警指标 为  SystemInfoBean 的所有字段
	 * 分别包括： CPU , 内存 ， 网卡， 磁盘 
	 * 预警分为：简单预警      高级预警      专业预警
	 * 简单预警：简单关联数字， 如 内存使用率>70% , CPU利用率 >30%
	 * 高级预警：包括时间段和日期，例如：周一到周五  7点到18点 CPU利用率>60% 预警
	 * 专业预警：使用规则引擎预警， 例如同时要求 剩余存储空间<500M 和 内存使用率>60%时预警
	 * @throws IOException
	 * @throws SigarException
	 * @throws InterruptedException
	 */
	@Scheduled(fixedRate = 1000*5 )  
    public void service() throws Exception {
		if(!taskRuninit){
			try{
				taskRuninit = true ;
				SystemInfoBean bean = new SystemInfoBean() ;
		//		JavaInformations systemInfo = new JavaInformations(null , true) ;
				
				long uptime = new RuntimeData().getStarttime().getTime() ;
				bean.setUptime(uptime) ;		//系统运行时长 单位是秒
				bean.setUpdatetime(new Date(uptime)) ;//系统启动时间
				bean.setOstype(new StringBuffer().append(System.getProperty("os.name")).append(" ").append(System.getProperty("os.version")).toString()) ;	//操作系统名称
				if(lastInfoBean==null){	//查询上次启动以来最后一次记录
					lastInfoBean = DataPersistenceService.getLastSystemInfo();
				}
				if(firstInfoBean == null){	//记录本次启动以来的第一次记录
					firstInfoBean = bean ;
				}
		//		bean.setHardware(sigar.getCpuPerc().toString()) ;	//操作系统架构
//				if(sigar.getCpuInfoList().length>0){
//					bean.setHardware(sigar.getCpuInfoList()[1].getModel());
//				}
//				CpuPerc cpu = sigar.getCpuPerc() ;
//				/**
//				 * CPU信息
//				 */
//				bean.setCpusys(cpu.getSys()*100.00) ;
//				bean.setCpuus(cpu.getUser()*100.00) ;
//				bean.setCpuidle(cpu.getIdle()*100.00) ;
//				bean.setCpunice(cpu.getNice()*100.00) ;
//				bean.setCpuwait(cpu.getWait()) ;
//				bean.setCpupercent((cpu.getUser()+cpu.getSys())*100.00) ;
				/**
				 * 当前目录存储占用的空间以及磁盘状况
				 */
//				FileSystemUsage file = sigar.getFileSystemUsage(new File(".").getAbsolutePath()) ;
//				bean.setDiskfree(file.getFree()) ;
//				bean.setDiskqueue(file.getDiskQueue()) ;
//				bean.setDiskread(file.getDiskReadBytes());
//				bean.setDiskwrite(file.getDiskWriteBytes()) ;
//				bean.setDisktotal(file.getTotal()) ;
//				bean.setDiskuse(file.getUsed()) ;
//				bean.setDiskusepercent(file.getUsePercent()*100.00);	//百分百
				
//				if(infobean!=null){
//					bean.setDiskreadspeed((bean.getDiskread() - infobean.getDiskread())/5) ;
//					bean.setDiskwritespeed((bean.getDiskwrite() - infobean.getDiskwrite())/5) ;
//				}
				
				
				/**
				 * 记录交换分区信息
				 */
////				Swap swap = sigar.getSwap();   
//				bean.setSwaptotal(swap.getTotal()) ;
//				bean.setSwapuse(swap.getUsed());
//				bean.setSwapfree(swap.getFree()) ;
//				bean.setSwappercent(swap.getTotal()==0 ?  0: ((int)((swap.getUsed()*1.0f/swap.getTotal()*1.0f)*10000)/100)) ;	//保留两位小数
//				if(infobean!=null){
//					bean.setSwappagein((swap.getPageIn()-infobean.getSwappagein())/5) ;
//					bean.setSwappageout((swap.getPageOut()-infobean.getSwappageout())/5) ;
//				}
//				
				
				/**
				 * JVM内存状态
				 */
				Runtime runtime = Runtime.getRuntime() ;
				bean.setJvmmemfree(runtime.freeMemory());
				bean.setJvmmemtotal(runtime.totalMemory());
				bean.setJvmmemuse(runtime.totalMemory()-runtime.freeMemory()) ;
				bean.setJvmmemusepercent(runtime.totalMemory()==0 ? 0 :  ((float)(runtime.totalMemory()-runtime.freeMemory())/(float)runtime.totalMemory()));
				/**
				 * 内存基本信息
				 */
				
//				Mem mem = sigar.getMem() ;
//				bean.setMemtotal(mem.getTotal()) ;
//				bean.setMemfree(mem.getFree()) ;
//				bean.setMemused(mem.getUsed()) ;
//				bean.setMempercent(mem.getUsedPercent()) ;
				
				/**
				 * 网络信息
				 */
				RuntimeData runtimeData = new RuntimeData();
				bean.setHostname(runtimeData.getHostname()) ;
				bean.setIpaddr(runtimeData.getIpaddr()) ;
				bean.setGroupid("") ;
				bean.setDatatype(RivuDataContext.SystemInfoType.R3QUERY_V50_SERVER.toString());
				bean.setPort(runtimeData.getPort());
//				String[] netConfigList = sigar.getNetInterfaceList() ;
//				for(int i=0 ; i<netConfigList.length; i++){
//					NetInterfaceConfig config = sigar.getNetInterfaceConfig(netConfigList[i]) ;
//					NetInterfaceStat stat = sigar.getNetInterfaceStat(config.getName()) ;
//					if(stat!=null && stat.getRxBytes()>0 && stat.getTxBytes()>0){
//						long txbytes = 0;
//						long rxbytes =0;
//						long txerrors =0;
//						long rxerrors =0;
//						long rxpackage =0;
//						long txpackage =0;
//						StringBuffer strb = new StringBuffer();
//						{
//							if(stat.getRxBytes()>0 && stat.getTxBytes()>0){
//								if(strb.length()<200){
//									if(strb.length()>0){
//										strb.append(",");
//									}
//									strb.append(config.getName()).append(":").append(stat.getSpeed());
//								}
//								txbytes += stat.getTxBytes() ;
//								rxbytes += stat.getRxBytes() ;
//								txerrors += stat.getTxErrors();
//								rxerrors += stat.getRxErrors();
//								txpackage += stat.getTxPackets();
//								rxpackage += stat.getRxPackets();
//								bean.setNetspeed(stat.getSpeed()) ;
//							}
//						}
//						
//						bean.setNetread(rxbytes);
//						bean.setNetwrite(txbytes);
//						bean.setNetname(strb.toString());
//						bean.setRxerrors(rxerrors);
//						bean.setTxerrors(txerrors) ;
//						bean.setTxpackage(txpackage) ;
//						bean.setRxpackage(rxpackage) ;
//						if(infobean!=null){
//							bean.setNetreadspeed((bean.getNetread()-infobean.getNetread())/5) ;
//							bean.setNetwritespeed((bean.getNetwrite()-infobean.getNetwrite())/5) ;
//							bean.setRxpackagespeed((bean.getRxpackage()-infobean.getRxpackage())/5) ;
//							bean.setTxpackagespeed((bean.getTxpackage()-infobean.getTxpackage())/5);
//						}
//						if(bean.getNetspeed()!=0){
//							bean.setNetuseprecent(bean.getNetspeed()==0 ? 0 : (100*((double)(bean.getNetreadspeed()+bean.getNetwritespeed())/(double)bean.getNetspeed()))) ;
//						}else{
//							bean.setNetuseprecent(0.0);
//						}
//						
//			  		}
//				}
				/**
				 * 保存系统基本信息
				 */
				DataPersistenceService.persistence(bean) ;
				infobean = bean ;
			}finally{
				taskRuninit = false; 
			}
		}
    }
	/**
	 * 服务名称
	 */
	public String getName(){
		return RivuDataContext.ServiceTypeName.SYSTEM_MONITOR.toString();
	}
	public static SystemInfoBean getLastInfoBean() {
		return lastInfoBean;
	}
	public static void setLastInfoBean(SystemInfoBean lastInfoBean) {
		SystemMonitorService.lastInfoBean = lastInfoBean;
	}
	public static SystemInfoBean getFirstInfoBean() {
		return firstInfoBean;
	}
	public static void setFirstInfoBean(SystemInfoBean firstInfoBean) {
		SystemMonitorService.firstInfoBean = firstInfoBean;
	}
}
