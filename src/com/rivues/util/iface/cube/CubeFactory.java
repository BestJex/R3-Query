package com.rivues.util.iface.cube;

public class CubeFactory {

	private static CubeInterface cube ;
	/**
	 * 
	 * @return
	 */
	public static CubeInterface getCubeInstance(){
		return cube!=null ? cube : (cube = new DatabaseCubeImpl()) ;
	}
}
