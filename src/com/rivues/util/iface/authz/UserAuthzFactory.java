package com.rivues.util.iface.authz;

import com.rivues.core.RivuDataContext;
import com.rivues.util.service.cache.CacheHelper;

public class UserAuthzFactory {
	
	private static UserAuthz userauthz;
	/**
	 * 获取用户认证的API
	 * @return
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public static UserAuthz getInstance(String orgi) throws Exception{
		String clazz = (String) CacheHelper.getDistributedDictionaryCacheBean().getCacheObject(RivuDataContext.SystemParamEnum.USERAUTHCLASS.toString(), orgi) ;
		return userauthz!=null ? userauthz : (userauthz = (UserAuthz) Class.forName(clazz!=null ? clazz : "com.rivues.util.iface.authz.R3InnerUserAuthzImpl").newInstance());
	}

}
