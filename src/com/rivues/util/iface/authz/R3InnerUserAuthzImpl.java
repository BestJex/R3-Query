package com.rivues.util.iface.authz;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.hadoop.security.UserGroupInformation;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.mapping.Array;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.Auth;
import com.rivues.module.platform.web.model.Organ;
import com.rivues.module.platform.web.model.Role;
import com.rivues.module.platform.web.model.User;
import com.rivues.module.platform.web.model.UserOrgan;
import com.rivues.module.platform.web.model.UserRole;
import com.rivues.util.RivuTools;

public class R3InnerUserAuthzImpl implements UserAuthz{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4594434516434875679L;

	@Override
	public User getUser(String userid , String orgi) {
		@SuppressWarnings("unchecked")
		List<User> userList = RivuDataContext.getService().findAllByCriteria(
				DetachedCriteria.forClass(User.class).add(
						Restrictions.and(Restrictions.or(Restrictions.eq("username", userid), Restrictions.eq("email", userid)) ,
								Restrictions.eq("orgi", orgi))));
		User loginUser = userList != null && userList.size() > 0 ? userList.get(0) : null ;
		/**
		 * 获得用户授权列表 ， 获得用户 所属的角色和 机构列表
		 */
		
		if(loginUser!=null){
			loginUser.setAuthList(RivuTools.getAuthListByRole(this.getUserRoleList(loginUser))) ;
			loginUser.setRoleList(RivuTools.getUserRoleList(loginUser, orgi)) ;
			loginUser.setOrganList(RivuTools.getUserOrganList(loginUser, orgi)) ;
		}
		
		return loginUser;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getRoles(String orgi) {
		return RivuDataContext.getService().findAllByCriteria(
				DetachedCriteria.forClass(Role.class).add(Restrictions.eq("orgi", orgi)));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	/**
	 * 根据资源id获取角色
	 */
	public List<Auth> getAuth(String resourceid, String orgi) {
		return RivuDataContext.getService().findAllByCriteria(
				DetachedCriteria.forClass(Auth.class).add(Restrictions.and(Restrictions.eq("resourceid", resourceid),
						Restrictions.eq("orgi", orgi))));
	}


	@Override
	public User getUserName(String username , String orgi) {
		@SuppressWarnings("unchecked")
		List<User> userList = RivuDataContext.getService().findAllByCriteria(
				DetachedCriteria.forClass(User.class).add(
						Restrictions.and(Restrictions.or(Restrictions.eq("username", username), Restrictions.eq("email", username)) ,
								Restrictions.eq("orgi", orgi))));
		User loginUser = userList != null && userList.size() > 0 ? userList.get(0) : null ;
		loginUser.setAuthList(RivuTools.getAuthListByRole(this.getUserRoleList(loginUser))) ;
		return loginUser;
	}


	@Override
	public boolean authentication(String username, String password , String orgi) {
		@SuppressWarnings("unchecked")
		List<User> userList = RivuDataContext.getService().findAllByCriteria(
				DetachedCriteria.forClass(User.class).add(
						Restrictions.and(Restrictions.eq("username", username),
								Restrictions.eq("orgi", orgi))));
		return userList != null && userList.size() > 0
				&& userList.get(0).getPassword().equals(RivuTools.md5(password)) ? true : false;
	}
	

	@Override
	public List<UserRole> getUserRoleList(User user) {
		return RivuDataContext.getService().findAllByCriteria(DetachedCriteria.forClass(UserRole.class).add(Restrictions.eq("userid",user.getId())));
	}

	@Override
	public List<User> getUserListByRole(String role, String orgi) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> getUserListByOrgan(String organ, String orgi) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isSystemManager(String orgi, String username) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isModelManager(String orgi, String username) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isReportManager(String orgi, String username) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDicManager(String orgi, String username) {
		// TODO Auto-generated method stub
		return false;
	}


	
}
