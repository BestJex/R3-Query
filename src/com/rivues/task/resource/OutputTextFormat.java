package com.rivues.task.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rivues.module.platform.web.model.JobDetail;

public class OutputTextFormat {
	private String id ;
	private String title ;
	private String parent ;
	
	private Map<String , Object> data = new HashMap<String , Object>();
	private List<SQLDataValue> sql = new ArrayList<SQLDataValue>();
	private JobDetail job ;
	public OutputTextFormat(JobDetail job){
		this.job = job ;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	public List<SQLDataValue> getSql() {
		return sql;
	}
	public void setSql(List<SQLDataValue> sql) {
		this.sql = sql;
	}
	public JobDetail getJob() {
		return job;
	}
	public void setJob(JobDetail job) {
		this.job = job;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
}
